msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: \n"

#: app/scripts/controllers/entrataMerci/ritiriWindCtrl.js:99
msgid "ACTIVITY ID"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:167
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:166
msgid "ALL CATEGORY"
msgstr ""

#: app/scripts/controllers/entrataMerci/adcCtrl.js:430
#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:498
#: app/scripts/controllers/entrataMerci/resoDaCampoCtrl.js:448
#: app/scripts/controllers/entrataMerci/rientroRiparazioneCtrl.js:760
#: app/scripts/controllers/entrataMerci/ritiriWindCtrl.js:1710
#: app/scripts/controllers/entrataMerci/ritiriWindCtrl.js:694
#: app/scripts/controllers/entrataMerci/swapRipairWindCtrl.js:544
#: app/scripts/controllers/entrataMerci/trasferimentoS2SCtrl.js:501
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:497
#: app/scripts/controllers/gestioneSpedizioni/gsOrdineSpedizioneCtrl.js:719
#: app/scripts/controllers/gestioneSpedizioni/gsRmaRiparazioneCtrl.js:744
#: app/scripts/controllers/gestioneSpedizioni/gsRmaWindCtrl.js:755
#: app/scripts/controllers/gestioneSpedizioni/gsStaffettaCtrl.js:686
#: app/scripts/controllers/gestioneSpedizioni/gsTrasferimentoS2SCtrl.js:775
msgid "Activity Id must be a number"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:346
#: app/views/entrataMerci/ddtTrasferimentiWind.html:400
#: app/views/gestioneSpedizioni/ddtUscita.html:345
#: app/views/gestioneSpedizioni/ddtUscita.html:399
msgid "Alert"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:262
#: app/views/gestioneSpedizioni/ddtUscita.html:261
msgid "All"
msgstr ""

#: app/views/common/actProjects.html:12
msgid "Catalog Type"
msgstr ""

#: app/views/entrataMerci/modalUI.html:214
#: app/views/entrataMerci/modalUI.html:256
#: app/views/entrataMerci/modalUI.html:297
#: app/views/entrataMerci/modalUI.html:334
msgid "Check"
msgstr ""

#: app/views/common/actcatalogo.html:19
msgid "Classification"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:191
#: app/views/entrataMerci/ddtTrasferimentiWind.html:218
#: app/views/gestioneSpedizioni/ddtUscita.html:190
#: app/views/gestioneSpedizioni/ddtUscita.html:217
msgid "Clear"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:326
#: app/views/gestioneSpedizioni/ddtUscita.html:325
msgid "Clear search"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:192
#: app/views/entrataMerci/ddtTrasferimentiWind.html:219
#: app/views/entrataMerci/modalUI.html:588
#: app/views/gestioneSpedizioni/ddtUscita.html:191
#: app/views/gestioneSpedizioni/ddtUscita.html:218
msgid "Close"
msgstr ""

#: app/views/entrataMerci/modalUI.html:213
#: app/views/entrataMerci/modalUI.html:255
#: app/views/entrataMerci/modalUI.html:583
msgid "Confirm"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:678
#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:679
#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:680
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:677
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:678
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:679
msgid "DDT NOT DEFINED"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:198
#: app/views/entrataMerci/ddtTrasferimentiWind.html:208
#: app/views/gestioneSpedizioni/ddtUscita.html:197
#: app/views/gestioneSpedizioni/ddtUscita.html:207
msgid "Data al"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:169
#: app/views/entrataMerci/ddtTrasferimentiWind.html:181
#: app/views/gestioneSpedizioni/ddtUscita.html:168
#: app/views/gestioneSpedizioni/ddtUscita.html:180
msgid "Data dal"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:138
#: app/views/entrataMerci/ddtTrasferimentiWind.html:147
#: app/views/gestioneSpedizioni/ddtUscita.html:137
#: app/views/gestioneSpedizioni/ddtUscita.html:146
msgid "Ddt"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtManagementCtrl.js:24
#: app/scripts/controllers/entrataMerci/ddtManagementCtrl.js:25
msgid "Ddt in entrata"
msgstr ""

#: app/scripts/controllers/gestioneSpedizioni/ddtManagementCtrl.js:11
#: app/scripts/controllers/gestioneSpedizioni/ddtManagementCtrl.js:12
msgid "Ddt in uscita"
msgstr ""

#: app/views/common/actcatalogo.html:6
msgid "Description"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:280
#: app/views/gestioneSpedizioni/ddtUscita.html:279
msgid "Destinatario"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:1016
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:1015
msgid "Download successful"
msgstr ""

#: app/scripts/alerts.js:35
#: app/scripts/alerts.js:47
#: app/scripts/alerts.js:69
msgid "Error detail"
msgstr ""

#: app/scripts/alerts.js:68
msgid "Error message"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:1029
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:1028
msgid "File not available"
msgstr ""

#: app/views/common/actProjects.html:8
#: app/views/common/actcatalogo.html:14
msgid "Final customer"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:164
#: app/views/gestioneSpedizioni/ddtUscita.html:163
msgid "Id Lotto"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:155
#: app/views/gestioneSpedizioni/ddtUscita.html:154
msgid "Id lotto"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:271
#: app/views/gestioneSpedizioni/ddtUscita.html:270
msgid "Indirizzo destinatario"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:286
#: app/views/gestioneSpedizioni/ddtUscita.html:285
msgid "Indirizzo mittente"
msgstr ""

#: app/index.html:100
msgid "Loading..."
msgstr ""

#: app/views/common/actmagazzini.html:8
msgid "Location type"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:295
#: app/views/gestioneSpedizioni/ddtUscita.html:294
msgid "Mittente"
msgstr ""

#: app/views/common/table-xy-scroll.html:69
msgid "Mostra/Nascondi Colonne"
msgstr ""

#: app/views/common/table-xy-scroll.html:137
msgid "Mostra:"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:402
#: app/views/gestioneSpedizioni/ddtUscita.html:401
msgid "No data found for set {{options.filtroSubcategory}} - {{dataSubTabAsBuilt.active.label}}"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:348
#: app/views/gestioneSpedizioni/ddtUscita.html:347
msgid "No found ddt"
msgstr ""

#: app/scripts/directives/etichetta-materiale.js:116
#: app/scripts/directives/etichetta-materiale.js:34
msgid "No template available for sticker type {{type}}"
msgstr ""

#: app/views/common/Emcatalogo.html:7
#: app/views/common/actcatalogo.html:8
msgid "P/N"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:52
#: app/views/gestioneSpedizioni/ddtUscita.html:52
msgid "PN"
msgstr ""

#: app/views/entrataMerci/modalUI.html:179
msgid "Per inviare una richiesta per la creazione di una voce a catalogo cliccare il pulsante, compilare e spedire la mail"
msgstr ""

#: app/views/entrataMerci/modalUI.html:587
msgid "Print label"
msgstr ""

#: app/views/common/actProjects.html:6
#: app/views/common/actcatalogo.html:12
#: app/views/entrataMerci/ddtTrasferimentiWind.html:80
#: app/views/gestioneSpedizioni/ddtUscita.html:79
msgid "Project"
msgstr ""

#: app/views/common/actProjects.html:11
#: app/views/common/actcatalogo.html:15
msgid "Property"
msgstr ""

#: app/views/common/actmagazzini.html:8
msgid "Region"
msgstr ""

#: app/views/common/table-xy-scroll.html:68
msgid "Reset filter and grid refresh"
msgstr ""

#: app/views/entrataMerci/modalUI.html:578
msgid "Restores"
msgstr ""

#: app/views/common/Emcatalogo.html:8
#: app/views/common/actcatalogo.html:9
msgid "SAP code"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:319
#: app/views/gestioneSpedizioni/ddtUscita.html:318
msgid "Search"
msgstr ""

#: app/views/entrataMerci/modalUI.html:153
msgid "Search And Select PN"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:60
#: app/views/gestioneSpedizioni/ddtUscita.html:60
msgid "Search material"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:89
#: app/views/gestioneSpedizioni/ddtUscita.html:88
msgid "Search project"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:239
#: app/views/gestioneSpedizioni/ddtUscita.html:238
msgid "Seleziona i magazzini"
msgstr ""

#: app/views/common/Emcatalogo.html:9
#: app/views/common/actcatalogo.html:7
msgid "Sirti code"
msgstr ""

#: app/views/common/actProjects.html:7
#: app/views/common/actcatalogo.html:13
msgid "Subproject"
msgstr ""

#: app/views/common/actcatalogo.html:18
msgid "Supplier"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:115
#: app/views/gestioneSpedizioni/ddtUscita.html:114
msgid "Tipo ddt"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:190
#: app/views/entrataMerci/ddtTrasferimentiWind.html:217
#: app/views/gestioneSpedizioni/ddtUscita.html:189
#: app/views/gestioneSpedizioni/ddtUscita.html:216
msgid "Today"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:106
#: app/views/gestioneSpedizioni/ddtUscita.html:105
msgid "Type ddt"
msgstr ""

#: app/scripts/directives/etichetta-materiale.js:111
#: app/scripts/directives/etichetta-materiale.js:29
msgid "Undefined sticker type"
msgstr ""

#: app/scripts/controllers/entrataMerci/ddtTrasferimentiWindCtrl.js:1034
#: app/scripts/controllers/gestioneSpedizioni/ddtUscitaCtrl.js:1033
msgid "Unknown error"
msgstr ""

#: app/scripts/app.js:319
msgid "Unknown error (HTTP status: {{errStatus}})"
msgstr ""

#: app/views/entrataMerci/modalUI.html:215
#: app/views/entrataMerci/modalUI.html:257
#: app/views/entrataMerci/modalUI.html:298
#: app/views/entrataMerci/modalUI.html:335
msgid "Verified"
msgstr ""

#: app/views/common/table-xy-scroll.html:145
msgid "record"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:40
#: app/views/gestioneSpedizioni/ddtUscita.html:40
msgid "{{options.openPreview  ? 'CLose preview' : 'Open preview'}}"
msgstr ""

#: app/views/entrataMerci/ddtTrasferimentiWind.html:34
#: app/views/gestioneSpedizioni/ddtUscita.html:34
msgid "{{options.openSearch  ? 'Ddt search' : 'Ddt list'}}"
msgstr ""
