��    B      ,  Y   <      �     �     �     �     �     �     �     �     �          	               $     4     <     E     I     X     f     r          �     �     �     �     �     �     �     �  
             %     .     F  X   N     �  /   �     �     �  s   �     _     k     s     |     �     �     �     �     �     �     �     �  
   	  
   	     	     #	     ,	     2	     ;	     R	  *   `	     �	     �	  ;   �	  3   �	  
  
          #  !   6     X     ^     d     r     {     �  
   �     �     �     �     �     �     �     �     �     �     �  "   
     -     >     R     d     s     |     �     �  
   �     �     �     �     �  b   �     U  4   e     �     �  s   �          &  
   /     :  (   B  
   k     v          �     �     �     �     �     �  	   �     �     �               )  /   <  
   l     w  <   ~  5   �     ?          <         ,   >            :   B   #   &           	         "   9                /       7                   .         A   =   '             -                     0   %   $   6         ;   +   *   5       @   )                     
             4       3   !      1   (          8           2                     ACTIVITY ID ALL CATEGORY Activity Id must be a number Alert All Catalog Type Check Classification Clear Clear search Close Confirm DDT NOT DEFINED Data al Data dal Ddt Ddt in entrata Ddt in uscita Description Destinatario Download successful Error detail Error message File not available Final customer Id Lotto Id lotto Indirizzo destinatario Indirizzo mittente Loading... Location type Mittente Mostra/Nascondi Colonne Mostra: No data found for set {{options.filtroSubcategory}} - {{dataSubTabAsBuilt.active.label}} No found ddt No template available for sticker type {{type}} P/N PN Per inviare una richiesta per la creazione di una voce a catalogo cliccare il pulsante, compilare e spedire la mail Print label Project Property Region Reset filter and grid refresh Restores SAP code Search Search And Select PN Search material Search project Seleziona i magazzini Sirti code Subproject Supplier Tipo ddt Today Type ddt Undefined sticker type Unknown error Unknown error (HTTP status: {{errStatus}}) Verified record {{options.openPreview  ? 'CLose preview' : 'Open preview'}} {{options.openSearch  ? 'Ddt search' : 'Ddt list'}} Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
Plural-Forms: nplurals=2; plural=(n != 1);
 ID ATTIVITA' TUTTE LE CATEGORIE Id attivita deve essere un numero Alert Tutti Tipo catalogo Verifica Classificazione Pulisci Ripristina Chiudi Conferma DDT NON DEFINITO Data al Data dal Ddt Ddt in entrata Ddt in uscita Descrizione Destinatario Scaricamento avvenuto con successo Dettaglio errori Messaggio di errore File non presente Cliente finale Id Lotto Id lotto Indirizzo destinatario Indirizzo mittente Loading... Tipo locazione Mittente Mostra/Nascondi colonne Mostra: Nessun dato trovato per il set  {{options.filtroSubcategory}} - {{dataSubTabAsBuilt.active.label}} Ddt non trovato Template non presente per il tipo etichetta {{type}} P/N PN Per inviare una richiesta per la creazione di una voce a catalogo cliccare il pulsante, compilare e spedire la mail Stampa etichetta Progetto Proprietà Regione Rimozione filtri e aggiornamento tabella Ripristina SAP code Cerca Cerca e  seleziona  PN Cerca materiale Cerca progetto Seleziona i magazzini Codice Sirti Sottoprogetto Fornitori Tipo ddt Oggi Tipo ddt Tipo etichetta non definita Errore sconosciuto Errore sconosciuto (HTTP status: {{errStatus}}) Verificato record {{options.openPreview  ? 'Chiudi preview' : 'Apri preview'}} {{options.openSearch  ? 'Ricerca Ddt' : 'Lista Ddt'}} 