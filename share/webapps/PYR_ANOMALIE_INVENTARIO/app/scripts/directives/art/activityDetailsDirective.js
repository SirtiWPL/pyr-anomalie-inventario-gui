'use strict';

function ModalModifyActivityCtrl($scope, $rootScope, $state, $stateParams, $q, $timeout, $http,$filter,
		focus, LoadTagsAsync, UiConfig, RenderPropertyLabel,
		ActivityFlowService,$window, ActivityPropertyTypesService,ActivityGetService,
		TempFileUpload, tempFileUploadRoute, _, Alert, DownloadService,
		gettextCatalog, $uibModalInstance, data,ActivityHistoryService,ActivityAttachmentsService,
		ActivityAttachmentService
//		permessiDettaglio , permessiInteractions , 
//		buildingPermits, buildingPermitsDetails, buildingPermitsInteractions,
//		buildingsAttachmentService, permitsAttachmentService, 
//		buildingDetails, buildingTransitions 
		) {
	/**
	 * Modifica il tab/sezione corrente
	 */
	$scope.setCurrentTab = function(i){
		$scope.currentTab = i;
	};
	
	$scope.currentTab = 0;

	$scope.alert = Alert;

	// spinner
	$scope.dettaglioCaricato=false;
	$scope.refreshing = false;

	// arriva dal parent
	$scope.permessoLavori = data.activity;
	var permesso = $scope.permessoLavori;

	$scope.idPermessoLavori = $scope.permessoLavori.id;
	// tipo attività
	var typeActivityArt = $scope.permessoLavori.info.type;
	// inizializzo l'attività
	$scope.activity = [];

	$scope.tabList = [ {
		'index': 0,
		'label': 'Permit summary & edit',
		'icon': 'fa fa-map-signs',
		'view': 'views/art/activity-details-riepilogo.html',
		'visibled': true
	},{
		'index': 1,
		'label': 'Elenco file',
		'icon': 'fa fa-files-o',
		'view': 'views/art/activity-details-files.html',
		'visibled': typeActivityArt !== 'BUILDING_LC'
	}, {
		'index': 2,
		'label': 'History',
		'icon': 'fa fa-history',
		'view': 'views/art/activity-details-riepilogo.html',
		'visibled': typeActivityArt !== 'BUILDING_LC'
	}/*, {
		'index': 2,
		'label': 'Attachments',
		'icon': 'paperclip',
	    'view': 'views/collaudo/ccs-dettaglio-attachments.html',
	    'visibled': typeActivityArt !== 'BUILDING_LC'
	}*/];

	/**
	 * Recupero il flusso di COLLAUDO_CCS
	 */
	$scope.activityTypeFlow = ActivityFlowService.getFlow( { TYPE: typeActivityArt, CONTEXT: 'ap' } );

	/**
	 * Recupero l'elenco delle ACTIONs del flusso 'PERMESSO_LAVORI'
	 */
	$scope.activityTypeActions = ActivityFlowService.enumActivityTypeActions( { TYPE: typeActivityArt, CONTEXT: 'ap' } );

	/**
	 * Recupero l'elenco degli STATI del flusso 'PERMESSO_LAVORI'
	 */
	$scope.activityTypeStatuses = ActivityFlowService.enumActivityTypeStatuses( { TYPE: typeActivityArt, CONTEXT: 'ap' } );

	/**
	 * Recupero la tipologia delle varie property
	 */
	$scope.activityTypeProperties = ActivityPropertyTypesService.get( { TYPE: typeActivityArt, CONTEXT	: 'ap' } );

	/**
	 * Recupero la activity a seconda del tipo attività sara necessario un set differente di parametri
	 * insistenti su servizi diversi
	 */

	/**
	 * Caricamento informazioni attività
	 */
	$scope.refresh = function(permesso) {
		$scope.refreshing = true;
		$scope.actions = undefined;
		var activity = [];
		var propertyGroups = [];
		//var id = permesso.id;
		/**
		 * per attivita di tipo PERMESSO_LAVORI
		 */
		if(typeActivityArt === 'PERMESSO_LAVORI'){
			//activity = permessiDettaglio.get( {
			activity = ActivityGetService.get( { ID: permesso.id });
			/**
			 * Caricamento activity property ART
			 */
			propertyGroups = ActivityHistoryService.get( { ID: permesso.id } );
		}

		/**
		 * Caricamento attività ART
		 */
//		Attendo che le promise vengano risolte
		$q.all( [ activity.$promise, propertyGroups.$promise  ] ).then(
				function() {
					/**
					 * Attendo che tutte le promise si risolvano
					 */
					$q.all([
							$scope.activityTypeFlow.$promise,
							$scope.activityTypeActions.$promise,
							$scope.activityTypeStatuses.$promise,
							$scope.activityTypeProperties.$promise
						]).then(function() {
							$scope.actions = $scope.activityTypeFlow[activity.info.status];
							$scope.actionsKeys = _.keys($scope.actions).sort(); // elenco delle chiavi sortato action
							$scope.processStages = $scope.activityTypeStatuses;
							$scope.selectAction();		// Resetta l'azione selezionata
							$scope.refreshing = false;
							$scope.dettaglioCaricato = true;
							activity.info.permitColor = 'info';
							activity.info.actionView = true;

							// injetto tipo visualizzazione
							if(activity.info.type === 'BUILDING_LC'){activity.info.actionView = false;}
							// inject del colore in base allo stato
							// logiche
							// stati finali CHIUSA stato non finale OTTENUTA (success)
							if(activity.info.status === 'CHIUSA' || activity.info.status === 'OTTENUTA'){activity.info.permitColor = 'success';}
							// stati finali RIFIUTATA stato non finale RESPINTA (danger)
							if(activity.info.status === 'RIFIUTATA' || activity.info.status === 'RESPINTA'){activity.info.permitColor = 'danger';}
							$scope.activity = angular.copy(activity);
							$scope.propertyGroups = angular.extend(propertyGroups);
						});
				},
				function(err) {
					$scope.refreshing = false;
					if (err.data){
						$scope.error = true;
						$scope.errorlabel = true;
						//$window.alert(err.data);
						$scope.alert.error(err.data, { ttl: 10000 });
					} else {
						//$window.alert('Unknown error');
						$scope.alert.error('Unknown error', { ttl: 10000 });
					}
				}
		);
	};
	// aggiornamento di tutto il modale
	$scope.refresh($scope.permessoLavori);

	/**
	 * Esegue il pre-caricamento dei possibili valori di una property
	 */
	var preloadPropertyValues = function (key) {
		var url = '';
		if ($scope.activityTypeProperties[key].context === 'Config') {
			url += UiConfig.getKey($scope.activityTypeProperties[key].contextKey);
		}
		url += $scope.activityTypeProperties[key].uri;
		$scope.transitionProperties[key] = null;
		$http({
			method: 'GET',
			url: url,
			params: { },
			withCredentials: true
		}).then(
				function(res) {
					$scope.activityTypeProperties[key].values = res.data;
				},
				function(err) {
					console.log(err);
				}
		);
	};

	/**
	 * Aggiunta voce empty nelle SELECT
	 */
	$scope.addNullToSelect = function(values, mandatory) {
		if(!mandatory) {
			if(values[0] !== '') {
				values.unshift('');
			}
		}
	};

	$scope.status = {
			openedDt: {}
		};
	// gestione ISODAT
	$scope.openDt = function(event, propertyName) {
		event.preventDefault();
		event.stopPropagation();
		$scope.status.openedDt[propertyName] = true;
	};


	//temp array for attachments logics
	$scope.attachments = [];
	/**
	 * Seleziona l'azione da compiere
	 */

	$scope.selectedAction = undefined;

	// selezione della action
	$scope.selectAction = function(action) {

		// Elimino i tmpFile degli attachment (se presenti da un precedente tentativo di step)
		if ($scope.attachments) {
			_.each($scope.attachments, function(attachment) {
				if (!attachment.removed) {
					$scope.removeAttachmentService(attachment);
				}
			});
		}

		$scope.attachments = [];

//		Imposto il modello dati a supporto dello step
		$scope.transitionData = {
				action: action,
				description: '',
				properties: {},
				attachments: []
		};

//		Se non è stata specificata nessuna azione esco
		if (!action) {
			$scope.selectedAction = undefined;
			return;
		}

		// Url della rotta dedicata al caricamento di file temporanei propedeutici allo step
		$scope.tmpUrl = tempFileUploadRoute('ap');
		// Servizio ($resource) per la rimozione degli allegati da passare al gestore degli upload
		$scope.removeAttachmentService = function(file) {
			// remove fisico
			TempFileUpload.remove({ ID: file.uuid, CONTEXT: 'ap' });
			// remove logico dell'elemento
			$scope.attachments = _.without($scope.attachments,file);
		};

		/**
		 * Inizializzazione transition properties
		 */
		$scope.inputTabIndex = 1000;		// Rappresenta l'indice iniziale da asegnare
		// alla proprietà tabindex dei controlli
		// della form
		$scope.transitionProperties = {};	// Struttura properties a supporto dello
		// step
		for (var j=0; j<$scope.actions[action].properties.length; ++j) {

//			Determino il nome della property corrente
			var key = $scope.actions[action].properties[j].name;

//			Se pa property non è più valida la skippo...
			if ($scope.actions[action].properties[j].expired) {
				continue;
			}
//			FIXME: se esiste la chiave 'prePopulate' assumo che si tratti di una activity
//			property e
//			forzo il prepopolamento a 'true'
//			DA RIMUOVERE DOPO L'AGGIUNTA DELLA CHIAVE ANCHE ALLE PROPERTY GROUPS
			if ( !$scope.actions[action].properties[j].hasOwnProperty('prePopulate') ) {
				$scope.actions[action].properties[j].prePopulate = true;
			}

//			Se è una property r/w imposto un tabindex;
			if (!$scope.actions[action].properties[j].readOnly && !$scope.actions[action].properties[j].hidden) {
				$scope.actions[action].properties[j].tabIndex = $scope.inputTabIndex++;
			}

//			Applico trasformazioni ai valori contenuti nelle property
			switch ($scope.activityTypeProperties[key].type) {

			case 'NUMBER':
//				FIXME: rimuovere customizzazione su PERCENTAGE_OF_COMPLETION in favore di un
//				type 'percentage'
				if ($scope.activity && $scope.activity.properties[key] && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = parseFloat($scope.activity.properties[key]);
				} else if ( key === 'PERCENTAGE_OF_COMPLETION' ) {
					$scope.transitionProperties[key] = 0;
				}
				break;

			case 'ISODAT':
				if ($scope.activity && $scope.activity.properties[key] && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = new Date($scope.activity.properties[key]);
				}
				break;

			case 'TAGS':
				if ($scope.activity && $scope.activity.properties[key] && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = [];
					var currentTags = angular.fromJson($scope.activity.properties[key]);
					for (var x=0; x<currentTags.length; ++x) {
						$scope.transitionProperties[key].push( { tag : currentTags[x] } );
					}
				}
				break;

			case 'MULTISELECT':
			case 'MOOKUP':
				if ($scope.activity && $scope.activity.properties[key] && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = angular.fromJson($scope.activity.properties[key]);
				}
				if ($scope.transitionProperties[key] && $scope.activityTypeProperties[key].layout === 'AUTOCOMPLETE') {
					var tmp = $scope.transitionProperties[key];
					$scope.transitionProperties[key] = [];
					angular.forEach(tmp, function(val) {
						this.push({name : val});
					}, $scope.transitionProperties[key]);
				}
				/*
				 * FIXME: rotta retrocompatibilità con layout TABLE!?!?
				 * if($scope.activityTypeProperties[key].layout === 'TABLE' &&
				 * angular.isDefined(relatedTextHiddenField)) { relatedTextHiddenField[key] =
				 * $scope.transitionProperties[key].length ? 'compiled' : ''; }
				 */
				break;

			case 'LOOKUP':
				if ($scope.activity && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = $scope.activity.properties[key];
				}
				if ($scope.transitionProperties[key] && $scope.activityTypeProperties[key].layout === 'AUTOCOMPLETE') {
					var tmp1 = $scope.transitionProperties[key];
					$scope.transitionProperties[key] = [{name : tmp1}];
				}
				break;

			default:
				if ($scope.activity && $scope.actions[action].properties[j].prePopulate) {
					$scope.transitionProperties[key] = $scope.activity.properties[key];
				}
			}

//			Modifico il template dei widtget
			switch ($scope.activityTypeProperties[key].type) {
			case 'LOOKUP':
			case 'MOOKUP':
//				Se si tratta di un lookup con scope in ('SYSTEM_PROPERTY',
//				'ACTIVITY_PROPERTY') valorizzo
//				la chiave values coi valori estratti dalla property 'target'
				if ( $scope.activity && $scope.activityTypeProperties[key].scope ) {
					var source = '';
					switch ($scope.activityTypeProperties[key].scope) {
					case 'SYSTEM_PROPERTY':
						source = $scope.activity.system.properties[ $scope.activityTypeProperties[key].target ];
						break;
					case 'ACTIVITY_PROPERTY':
						source = $scope.activity.properties[ $scope.activityTypeProperties[key].target ];
						break;
					}
					$scope.activityTypeProperties[key].values = [];
					if ( source ) {
						try {
							var list = angular.fromJson($scope.activity.properties[ $scope.activityTypeProperties[key].target ]);
							for (var y=0; y<list.length; ++y) {
								var propertyName = $scope.activityTypeProperties[key].target;
								$scope.activityTypeProperties[key].values[y] = {};
								$scope.activityTypeProperties[key].values[y][propertyName] = list[y];
							}
						} catch (e) {
							// Il contenuto della property non era un json valido
						}
					}
				}

//				Se il layout è SELECT precarico i possibili valory dall'URL specificato dal
//				property-type
				if ($scope.activityTypeProperties[key].uri && $scope.activityTypeProperties[key].layout === 'SELECT') {
					preloadPropertyValues(key);
				}

				break;

			case 'POPUP':
				if(!$scope.actions[action].properties[j].mandatory &&
						$scope.actions[action].properties[j].prePopulate &&
						$scope.activity.properties[key] !== null) {
					$scope.activityTypeProperties[key].values.unshift('');
				}
				break;

			default:
				break;
			}
		}

//		Individua la property su cui trasferire il focus (in difetto sulla
//		descrizione attività)
		var fieldId = 'inputStepDescription';
		if ($scope.actions[action].properties.length > 0) {
			var firstProperty = _.find($scope.actions[action].properties, function(p) {
				return !p.hidden && !p.readOnly;
			});
			if (firstProperty) {
				fieldId = 'input' + firstProperty.name;
			}
		}

//		Azione selezionata
		$scope.selectedAction = action;
		$timeout(function() {
			focus(fieldId);
		});
	};

	/**
	 * Step attivita COLLAUDO_CCS
	 */
	$scope.step = function(permesso) {
		console.log(permesso);
		$scope.waitForStep = true;

//		Esegue una copia per evitare che le normalizzazioni invalidino i controlli
//		del form

		var transitionData = angular.extend($scope.transitionData);

//		Normalizzo le property
		angular.forEach($scope.transitionProperties, function(value, key) {
			switch ($scope.activityTypeProperties[key].type) {
			case 'TAGS':
				var tags = [];
				for (var i=0; i<value.length; ++i) {
					tags.push(value[i].tag);
				}
				transitionData.properties[key] = angular.toJson(tags);
				break;
			case 'MULTISELECT':
			case 'MOOKUP':
//				Estraggo il valore keyProperty da ogni elemento dell'array di object
				if($scope.activityTypeProperties[key].layout === 'AUTOCOMPLETE') {
					var tmp = value;
					value = [];
					angular.forEach(tmp, function(val) {
						this.push(val[$scope.activityTypeProperties[key].keyProperty]);
					}, value);
				}
				transitionData.properties[key] = angular.toJson(value);
				break;
			case 'LOOKUP':
//				Estraggo il valore keyProperty dall'elemento 0 dell'array di object
				if($scope.activityTypeProperties[key].layout === 'AUTOCOMPLETE') {
					if (angular.isDefined(value[0])) {
						transitionData.properties[key] = value[0][$scope.activityTypeProperties[key].keyProperty];
					}
				} else {
					transitionData.properties[key] = value;
				}
				break;
			default:
				transitionData.properties[key] = value;
			}

		});

		angular.forEach($scope.actions[$scope.selectedAction].properties, function(prop) {
			var key = prop.name;
			var mandatory = prop.mandatory;
			var hidden = prop.hidden;
			/*
			 * if(!$scope.activity) { // se sono in creazione attività le origProperties non
			 * sono definite (oggetto vuoto), // quindi definisco le json-like come stringa
			 * contentente un array vuoto, le altre come null switch
			 * ($scope.activityTypeProperties[key].type) { case 'TAGS': case 'MULTISELECT':
			 * case 'MOOKUP': $scope.origProperties[key] = '[]'; break; default:
			 * $scope.origProperties[key] = null; } }
			 */

//			normalizzo le properties che arrivano dal form
			if( angular.isUndefined(transitionData.properties[key]) || transitionData.properties[key] === null || transitionData.properties[key] === '') {
				switch ($scope.activityTypeProperties[key].type) {
				case 'TAGS':
				case 'MULTISELECT':
				case 'MOOKUP':
					transitionData.properties[key] = '[]';
					break;
				default:
					transitionData.properties[key] = null;
				}
			} else if(angular.isNumber(transitionData.properties[key])) {
				transitionData.properties[key] = transitionData.properties[key] + '';
			}

//			tute le property hidden non devono essere submittate
			if (hidden){
				delete transitionData.properties[key];
			} else if(!mandatory) {
//				tutte le property mandatory vengono submittate, le opzionali solo se
//				modificate
				if (angular.equals(transitionData.properties[key], $scope.activity.properties[key])) {
					delete transitionData.properties[key];
				}
			}

		});

		if ( $scope.activity ) {
			transitionData.description = transitionData.description || '-';
			/*
			 * if($scope.actionAssignable) { options.destUser = $scope.formValues.destUser;
			 * if($scope.showAssignNotification) { options.assignNotification =
			 * $scope.formValues.assignNotification; } }
			 */
		} /*
		 * else { service = ActivityCustomCreateService; params = { ACTIVITY_TYPE:
		 * $scope.activityType }; options = { description:
		 * ($scope.formValues.stepDescription ? $scope.formValues.stepDescription :
		 * '-'), properties: properties }; }
		 */
		/*
		 * if ( $rootScope.attachments.length > 0 ) { options.attachments = []; for (var
		 * a=0; a < $rootScope.attachments.length; ++a) { if
		 * (!$rootScope.attachments[a].removed)
		 * options.attachments.push($rootScope.attachments[a].uuid); } }
		 */


		/**
		 * Estrae dall'array degli attachment solo i valori della chiave uuid in quanto lo step si aspetta un array di stringhe
		 */
/* 		transitionData.attachments = _.map(
			_.where($rootScope.attachments, { status: 'success' }), function(o) {
				return o.uuid;
			});
 */

		_.each($scope.attachments, function(a) {
			transitionData.attachments.push(a.uuid);
		});

//		Step attività
		ActivityHistoryService.step(
			{ ID: $scope.permessoLavori.id },
			transitionData,
			function() {
				$scope.refresh($scope.permessoLavori);
				$scope.waitForStep = false;
				$uibModalInstance.close({ok : true , data : data});
			},
			function(err) {
				$scope.waitForStep = false;
				if (err.data){
					$scope.error = true;
					$scope.errorlabel = true;
					//$window.alert(err.data);
					$scope.alert.error(err.data, { ttl: 10000 });

					$uibModalInstance.close({ok : false , err : err.data || err});
				} else {
					//$window.alert('Unknown error');
					$scope.alert.info('Unknown error', { ttl: 10000 });
				}
			}
		);
		
	};

	$scope.close = function () {
		$uibModalInstance.dismiss('cancel');
	};


	/**
	 * Esegue il download dell'allegato
	 */
	$scope.activityDownloadFile = function(attachment) {
		// non procedo se il download del file è ancora in corso
		if (attachment.ongoing) { return; }

		attachment.ongoing = true;
		// Esegue il download dell'allegato invocando il servizio
		new DownloadService(
			ActivityAttachmentService.download,
			{
				ID: permesso.id,
				TRANSITION_ID: attachment.transitionId,
				SEQUENCE: attachment.sequence
			},
			function() {
				$scope.alert.success(gettextCatalog.getString('Download successful'));
				$timeout(function() {
					attachment.ongoing = false;
				}, 500);
			},
			function(err) {
				/*
				 * Gestisco l'errore come sempre
				 */
				$timeout(function() {
					attachment.ongoing = false;
					if (err.data){
						$scope.alert.error(err.data || err);
						return;
					} else {
						$scope.alert.error(gettextCatalog.getString('Unknown error'));
					}
				}, 500);
			}
		);
	};
}

function ActivityDettaglioHistoryCtrl($scope, $rootScope, $state, $stateParams, $q,
	_, permessiInteractions, buildingPermitsInteractions , buildingTransitions ,ActivityHistoryService) {

	var permesso = $scope.permessoLavori;
	console.log(permesso);
	$scope.refreshing = false;

	$scope.activity = undefined;
	$scope.propertyGroups = undefined;

	$scope.toggleReverseOrder = false;
	$scope.history = [];
	/**
	 * Caricamento storia attività
	 */
	$scope.refresh = function(permesso) {
		var tempHistory = [];
		$scope.refreshing = true;
		/**
		 * per attivita di tipo PERMESSO_LAVORI
		 */
		var history;

		// Rotta di activity history
		history =  ActivityHistoryService.get( {
			ID: permesso.id
		} );
			
		// Attendo la risoluzione di tutte le promise
		history.$promise.then(
			function(res) {
				_.each(res,function(s){
					if(s.attachments && s.attachments.length > 0 ){
						_.each(s.attachments,function(attach,i){
							// in caso di history injetto gli elementi mancanti nella struttura
							// per il download del file in moda da essere in linea con la chiamata centralizzata
							// nel controlle padre
							attach.ongoing = 		false;
							attach.sequence = 		i;
							attach.transitionId =	s.id;
							attach.transitionDate = s.transitionDate;
						});
					}
					tempHistory.push(s);
				});
				$scope.history = angular.extend(tempHistory);
				$scope.refreshing = false;
			},
			function(err) {
				$scope.refreshing = false;
				if (err) {
					$scope.error = true;
					$scope.errorlabel = true;
					$scope.alert.error(err.data||err);
				} else {
					$scope.alert.error('Unknown error');
				}
		});

	};

	$scope.refresh($scope.permessoLavori);

	$scope.downloadFile= function(event){
		$scope.activityDownloadFile(event);
	};
}

function ActivityDettaglioFilesCtrl($scope, $rootScope, $resource, $state, $stateParams, $timeout, $q,gettextCatalog,
		attachmentsList, buildingAttachmentsList,_,ActivityAttachmentsService) {

	var permesso = $scope.permessoLavori;
	console.log(permesso);
	$scope.refreshing = false;
	$scope.files = [];
	/**
	 * Recupera l'elenco degli allegati dell'attività corrente
	 */
	$scope.refresh = function(permesso) {
		var filesList;
		$scope.refreshing = true;
		/**
		 * per attivita di tipo PERMESSO_LAVORI
		 */

		// Rotta di activity history
		filesList =  ActivityAttachmentsService.get( { ID: permesso.id } );

		// Attendo la risoluzione della promise
		filesList.$promise.then(function(res) {
			_.each(res,function(f){
				f.ongoing = false;
			});
			$scope.files = angular.extend(res);
			$scope.refreshing = false;
		}, function(err) {
			$scope.refreshing = false;
			if (err.data) {
				$scope.error = true;
				$scope.errorlabel = true;
				$scope.alert.error(err.data||err);
			} else {
				$scope.alert.error('Unknown error');
			}
		});

	};

	$scope.refresh($scope.permessoLavori);

	$scope.downloadFile= function(event){
		$scope.activityDownloadFile(event);
	};

}

/**
 * Visualizza le Activity Property in funzione del tipo
 */
function activityProperty() {
	return {
		restrict: 'E',
		scope: {
			value : '=',
			type : '@',
		},
		templateUrl: 'views/art/activity.property.html',
		controller: function($scope) {

			$scope._type = '_DEFAULT';
			$scope._value = angular.copy($scope.value);
			var i = 0;
			switch($scope.type) {
				case 'ISODAT':
				case 'MEMO':
				case 'BOOL':
				case 'EMAIL':
					$scope._type = $scope.type;
					break;
				case 'LOOKUP':
					if(angular.isArray($scope._value) && angular.isObject($scope._value[0])) {
						// tipo_ui LOOKUP layout AUTOCOMPLETE
						$scope._value = $scope._value[0].name;
					}
					break;
				case 'MOOKUP':
					$scope._type = $scope.type;
					$scope._value = angular.fromJson($scope._value);
					for(i=0; i < $scope._value.length; i++) {
						if(angular.isObject($scope._value[i])) {
							$scope._value[i] = $scope._value[i].name;
						}
					}
					break;
				case 'TAGS':
					$scope._type = $scope.type;
					$scope._value = angular.fromJson($scope._value);
					for(i= 0; i < $scope._value.length; i++) {
						if(angular.isObject($scope._value[i])) {
							$scope._value[i] = $scope._value[i].tag;
						}
					}
					break;
			}

		}
	};
}



angular.module('guiPyrAnomalieInventario')
	.controller('ModalModifyActivityCtrl', ModalModifyActivityCtrl)
	.controller('ActivityDettaglioHistoryCtrl', ActivityDettaglioHistoryCtrl)
	.controller('ActivityDettaglioFilesCtrl', ActivityDettaglioFilesCtrl)
	.directive('activityProperty', activityProperty);

