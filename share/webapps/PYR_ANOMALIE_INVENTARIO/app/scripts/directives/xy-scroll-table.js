'use strict';
	/**
	 * New node file
	 */
	/**
	 * This directive listens for windows resize and fires a broadcast event 
	 */
angular.module('guiPyrAnomalieInventario')

	.directive('xyScrollTable', function ($window) {
		return {
			restrict : 'AE',
			scope:true,
			template:'<div ng-include="\'views/common/table-xy-scroll.html\'"></div>',
			link:function (scope, element) {
				// inserita in direttiva la numerosità per pagina
				// condivisa per tutte le griglie
				scope.pager = [2, 5, 10, 20, 50,75,100];
				
				function resize () {
					var windowHeight = window.innerHeight;
					var windowWidth  = window.innerWidth;
					scope.windowHeight = windowHeight;
					scope.windowWidth  = windowWidth;
					//** If want to apply style on element, can do something like:
					if(scope.type === 'body'){scope.over = 'auto';}
					var elemStyle = {
							'width': parseInt(windowWidth) + 'px',
							'Height': parseInt(windowWidth) + 'px',
							'min-width': parseInt(windowWidth) + 'px',
							'max-height': parseInt(windowWidth) + 'px'
					};
					element.css(elemStyle);
				}
				
				scope.$watchGroup(['windowWidth' , 'windowHeight'], function(){ resize();});
				
				$window.addEventListener('resize', function() {
					resize();
				});
				//** On resize
				window.onresize = function () {
					resize();
					scope.$apply();
				};
			}
		};
	})
	
	
.directive('resize', function ($window) {
		return {
			restrict : 'AE',
			scope: {
				confHeader: '=',
				type:'@',
				field:'@'
			},
			link:function (scope, element) {
				if (!scope.confHeader) {
					return;
				}
				
				function resize () {
					var windowHeight = window.innerHeight;
					var windowWidth  = window.innerWidth;
					scope.windowHeight = windowHeight;
					scope.windowWidth  = windowWidth;
					//** If want to apply style on element, can do something like:
					if(scope.type === 'body'){scope.over = 'auto';}
					var elemStyle = {
							width: parseInt(scope.confHeader[scope.field].width) + 'px',
							'min-width': parseInt(scope.confHeader[scope.field].width) + 'px',
							'max-width': parseInt(scope.confHeader[scope.field].width) + 'px',
							'white-space': 'normal',
							overflow : scope.over
							//display: 'table-row'
							//height: windowHeight + 'px'
					};
					element.css(elemStyle);
				}
				
				scope.$watchGroup(['windowWidth' , 'windowHeight'], function(){ resize();});
				
				$window.addEventListener('resize', function() {
					resize();
				});
				//** On resize
				window.onresize = function () {
					resize();
					//scope.$apply();
				};
			}
		};
	})
	;
	