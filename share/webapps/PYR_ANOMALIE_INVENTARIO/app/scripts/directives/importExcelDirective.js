'use strict';
	/**
	 * New node file
	 */
	/**
	 * This directive listens for windows resize and fires a broadcast event 
	 */
angular.module('guiPyrAnomalieInventario')
.directive('fileread', [function () {
	
	/* global XLSX */
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
  return {
    scope: {
      opts: '='
    },
    link: function ($scope, $elm) {
      $elm.on('change', function (changeEvent) {
        var reader = new FileReader();
        
        reader.onload = function (evt) {
          $scope.$apply(function () {
            var data = evt.target.result;
						
						var files = changeEvent.target.files;
            
            var workbook = XLSX.read(data, {type: 'binary'});
            
            var headerNames = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
            
            data = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]]);
            
            $scope.opts.columnDefs = [];
            headerNames.forEach(function (h) {
              $scope.opts.columnDefs.push({ field: h });
            });
            
            $scope.opts.data = data;
            $scope.opts.nomeFile = files[0].name;
            //$elm.val(null);
          });
        };
        
        reader.readAsBinaryString(changeEvent.target.files[0]);
      });
    }
  };
	
	// jscs:enable requireCamelCaseOrUpperCaseIdentifiers
}]);