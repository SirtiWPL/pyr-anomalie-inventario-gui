'use strict';

angular.module('version', [
  'version.interpolate-filter',
  'version.version-directive'
])

.value('version', '0.11.0-4'); // DON'T EDIT THIS LINE MANUALLY
