'use strict';

/**
 * @ngdoc service
 * @name advlog.uiServices
 * @description # uiServices Factory in the advlog.
 */

angular
		.module('uiServices', [])

		.factory('KeepAlivePageService', function($resource, $rootScope) {
			return function() {
				return $resource($rootScope.config.AdvlogKeepAlivePage, {}, {
					// aggiungo una parametro random per evitare caching
					get : {
						method : 'GET',
						params : {
							k: Math.floor(Math.random() * 10000001)
						},
						withCredentials : true,
						skipAuthorization : true
					}
				}, {});
			};
		})

		.factory(
				'RefreshSessionInterceptor',
				function($rootScope, $timeout, KeepAlivePageService) {
					return {
						'response' : function(response) {
							// 
							if ($rootScope.keepAliveTimeout) {
								return response.data;
							}
							// effettuo una chiamata a una pagina per effettuare
							// il refresh della sesione ART
							(new KeepAlivePageService()).get(
								function() {
									// tutto ok, posso continuare
									$rootScope.keepAliveTimeout = true;
									$timeout(function() {
										$rootScope.keepAliveTimeout = false;
									}, 60*1000); 
								},
								function() {
									// la sessione di ART è scaduta,
									// redirigo alla pagina di login
									window.location.replace($rootScope.config.AdvlogHomePage);
								}
							);
							return response.data;
						}
					};
				})
		
		.factory(
				'CheckAuthInterceptor',
				function($rootScope, $http, $q) {
					return {
						'response' : function(response) {
							// tutto ok, posso continuare
							return response.data;
						},
						'responseError' : function(rejection) {
							if (rejection.status === 401) {
								// su ADCPWS la sessione di è scaduta, redirigo
								// alla pagina di login
								window.location
										.replace($rootScope.config.AdvlogHomePage);
							}
							return $q.reject(rejection);
						}
					};
				})
				
		.factory(
				'CheckAuthAndRefreshSessionInterceptor',
				function($rootScope, $http, $q, RefreshSessionInterceptor, CheckAuthInterceptor) {
					return {
						'response': function (response) {
							return RefreshSessionInterceptor.response(response);
						},
						'responseError': function(response){
							return CheckAuthInterceptor.responseError(response);
						}
					};
				})

		.factory('ConfigService', function($resource) {
			return $resource('config.json.html', {}, {
				get : {
					method : 'GET',
					params : {},
					withCredentials : true,
					skipAuthorization : true
				}
			}, {});
		})
		
		.factory('UiConfig', function($rootScope) {
			return {
				AdvlogHomePage:function(){ return $rootScope.config.AdvlogHomePage; },
				modulo: function(){ return $rootScope.module; },
				rollBack : function(){ return $rootScope.config.rollback;},
				getKey: function(key) {
					//FIXME: per ora gestiamo il config ad un solo livello
					return $rootScope.config[key];
				}
			};
		})
		
		
		.factory('ActivityViewServices', function($httpParamSerializer, $rootScope) {
			function getUrl(params){
				// URL root
				var url = $rootScope.config.AdvlogHomePage + '/PYR_MTZ_NOC_MASTER/VEDI_ATTIVITA/index.html';
				 var serializedParams = $httpParamSerializer(params);
				if (serializedParams.length > 0) {
					url += ((url.indexOf('?') === -1) ? '?' : '&') + serializedParams;
				}
				return url;
			}
			return {getUrl:getUrl};
		})
		
		.factory('EnquireService',['$window',function($window){
			return $window.enquire;
		}])

		.service('global',['$rootScope','EnquireService','$document',function($rootScope,EnquireService,$document){
	
			this.settings = {
					fixedHeader:!0,
					headerBarHidden:!0,
					leftbarCollapsed:!1,
					leftbarShown:!1,
					rightbarCollapsed:!1,
					fullscreen:!1,
					layoutHorizontal:!1,
					layoutHorizontalLargeIcons:!1,
					layoutBoxed:!1,
					showSearchCollapsed:!1
			};
			var brandColors = {'default':'#ecf0f1',
					inverse:'#95a5a6',
					primary:'#3498db',
					success:'#2ecc71',
					warning:'#f1c40f',
					danger:'#e74c3c',
					info:'#1abcaf',
					brown:'#c0392b',
					indigo:'#9b59b6',
					orange:'#e67e22',
					midnightblue:'#34495e',
					sky:'#82c4e6',
					magenta:'#e73c68',
					purple:'#e044ab',
					green:'#16a085',
					grape:'#7a869c',
					toyo:'#556b8d',
					alizarin:'#e74c3c'
			};
			this.getBrandColor = function(name){
				return brandColors[name] ? brandColors[name] : brandColors['default'];
			};
			
			$document.ready(function(){ 
				EnquireService.register('screen and (max-width: 767px)',
					{ 
						match:function(){ $rootScope.$broadcast('globalStyles:maxWidth767',!0);},
						unmatch:function(){$rootScope.$broadcast('globalStyles:maxWidth767',!1);}
					}
				);
				
			});
			this.get = function(key){ return this.settings[key]; };
			this.set = function(key,value){ 
				this.settings[key] = value; 
				$rootScope.$broadcast('globalStyles:changed',{ key:key, value:this.settings[key] });
				$rootScope.$broadcast('globalStyles:changed:'+key,this.settings[key]);
			};
			
			this.values = function(){ return this.settings; }; 
		}])
		
;

