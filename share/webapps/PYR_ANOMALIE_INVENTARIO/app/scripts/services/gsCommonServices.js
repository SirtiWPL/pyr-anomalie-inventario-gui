'use strict';

angular
	.module('gsCommonServices', [])
	.factory('printTableMock', function($resource) {
		return $resource('./mock-data/fakedata.json',{ }, {
			getData: {method:'GET', isArray: false}
		});
	})
	/*  INVENTARIO - COMMON - GRID - MAGAZZINI OPERATORE  */
	.factory('getListaMagazziniOperatoreGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrInventario/common/listaMagazziniOperatore/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	.factory('azRisolviAnomaliaPostInv', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrInventario/common/pkgInventario/action/RisolviAnomaliaPostInv', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })

;
	


	


    