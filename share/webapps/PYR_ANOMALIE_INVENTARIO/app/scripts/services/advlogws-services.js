'use strict';

/**
 * @ngdoc service
 * @name advlogwsServices
 * @description # advlogwsServices.
 */

angular.module('advlogwsServices', [])

	.value('ADVLOGWSCanAccessObj', {})

	.run(function($rootScope, $q, ADVLOGWSCanAccessObj) {
		$rootScope.ADVLOGWS = {
			profilo: {
				gruppi: []
			},
			err: undefined
		};
		ADVLOGWSCanAccessObj.promise = $q.defer();
	})

	.factory('ADVLOGWSConfigService', function($resource, _, CheckAuthInterceptor, $rootScope) {
		return function() {
			return $resource($rootScope.config.AdvlogwsAppService + 'config', {}, {
				get : {
					method : 'GET',
					params : {},
					withCredentials : true,
					interceptor : CheckAuthInterceptor
				}
			}, {
				stripTrailingSlashes : true
			});
		};
	})

	.factory('ADVLOGWSProfile', function($rootScope, ADVLOGWSCanAccessObj) {
		return {
			canAccess: function() {
				return ADVLOGWSCanAccessObj.promise.promise;
			},
			gruppi: function() { return $rootScope.ADVLOGWS.profilo.gruppi; },
			err: function() { return $rootScope.ADVLOGWS.err; }
		};
	})
	
	.factory('ADVLOGWSConfig', function($rootScope, ADVLOGWSCanAccessObj) {
		return {
			set: function(config, err) {
				if(err) {
					ADVLOGWSCanAccessObj.promise.reject('Not Authorized');
					$rootScope.ADVLOGWS.err = err;
					return;
				}
				$rootScope.ADVLOGWS.profilo.gruppi        = config.profilo.gruppi;

				ADVLOGWSCanAccessObj.promise.resolve(true);
			}
		};
	})
	
	.factory('MaterialePyramide', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/materiale/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	.factory('MaterialiQta', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/materialeQta/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
    
	.factory('CatalogoPyramide', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/catalogo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	.factory('MaterialePyramideMR', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/materiale/grid_mr', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/* stesso PN o SP e equivalenti */

	.factory('CatalogoEquivalenti', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/catalogo/equivalente', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/* stesso PN o SP e equivalenti e compatibili  e  */
	.factory('CatalogoCompatibili', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/catalogo/compatibile', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })		
	/* stesso PN o SP */
	.factory('CatalogoByPnOrSapCode', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/catalogo/pnOrSapcode', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/* ricerca magazini sirti */
	.factory('getMagazziniSirti', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/magazziniSirti/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	.factory('getUbicazioniPyrSap', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/ubicazioniPyrSap/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })			
	.factory('checkSN', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogHomePage + 'pyramide/ws_check_sn.html', {}, {
				getData : {
						method : 'GET',
						isArray: true,
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	.factory('listaProgettiCatalogo', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/listaProgettiCatalogo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	.factory('listaRiparatori', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/listaRiparatori/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	// glsSpedizioniWind rotta per la definizione delle griglie
	.factory('glsSpedizioniWind', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/glsSpedizioniWind/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	
	// glsReportWind rotta per la definizione del report
	.factory('glsReportWind', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/glsSpedizioniWind/export/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	
	// task BPIOA-331: nuova rotta per TILE - CAMBIO UBICAZIONE -> grid dei materiali selezionati
	.factory('getMaterialiSelezCambioUbicazioneGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/cambioUbicazione/materiali/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	//nuova rotta per TILE - CAMBIO UBICAZIONE -> grid dei codici sirti da selezionare
	.factory('getCodiciSirtiCambioUbicazioneGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/cambioUbicazione/codiciSirti/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	//nuova rotta per TILE - CAMBIO UBICAZIONE -> azCambioLocazione
	.factory('azCambioLocazioneCambioUbicazione', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/cambioUbicazione/azCambioLocazione', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	//http://dvmas003.sirtisistemi.net:10095/advlogws/core/pyr/gestioneRiparazioni/AttivaRiparazione/sirti/getIndirizzoRiparatore?RIPARATORE=ALCATEL-LUCENT FRANCE DE GAULLE&ID_SISTEMA=333297
	.factory('getAddressByRepairer', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/getIndirizzoRiparatore', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})	
	
	.factory('tempFileUploadRoute', function($rootScope) {
		return function() {
			return $rootScope.config.AdvlogwsAppService + '/pyr/tmpfiles';
		};
	})

	.factory('TempFileUpload', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
		return $resource( $rootScope.config.AdvlogwsAppService + '/pyr/tmpfiles/:ID',
			{},
			{
			remove: {
				method: 'DELETE',
				params: {},
				withCredentials: true,
				interceptor: CheckAuthAndRefreshSessionInterceptor
			}
			},
			{
				stripTrailingSlashes: true
			}
		);
	})
	
	.factory('sendMailServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope, UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/sendMail', {}, {
				send : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
	})
	
//	.factory('DdtDownloadService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor, getFilenameFromHeader) {
//		//http://dvmas003.sirtisistemi.net:10103/stampa_ddt_upload/stampaDDT.html?id_attivita=25712715&ddt_numero=1013CT/95625/2018
//		//http://dvmas002.sirtisistemi.net:10086/stampa_ddt_upload/stampaDDT.html?id_attivita=25712715&ddt_numero=1013CT/95625/2018
//		//http://dvmas002.sirtisistemi.net:10086/stampa_ddt_upload/ws_stampa_ddt.html?ddt_numero=1013CT%2F95625%2F2018&id_attivita=25712715
//		//return $resource($rootScope.config.AdvlogwsAppService + '/pyramide/ws_check_sn.html',
//		//http://dvmas002.sirtisistemi.net:10086/attivita/dump_allegati.cgi?file_s=25022925_20180307144027-0000.tar.gz&file_c=MILANO_LORENTEGGIO_95788_2018.pdf
//		//http://dvmas002.sirtisistemi.net:10086/stampa_ddt_upload/dump_allegati.cgi?file_s=25022925_20180307144027-0000.tar.gz&file_c=MILANO_LORENTEGGIO_95788_2018.pdf
//		return $resource('dvmas002.sirtisistemi.net:10086/stampa_ddt_upload/dump_allegati.cgi',{},
//			{
//				download: {
//					method: 'GET',
//					//responseType: 'blob',
//					params: {},
//					//withCredentials: true,
//					interceptor: CheckAuthAndRefreshSessionInterceptor,
//					// Trasformo la response per iniettare il nome del file dagli header
//					transformResponse: function(data, headers,status) {
//						console.log(data);
//						if (status !== 200) {
//							return angular.fromJson( data );
//						}
//						return {
//							data: data,
//							filename: getFilenameFromHeader(headers)
//						};
//					}
//				}
//			},
//			{
//				stripTrailingSlashes: true
//			}
//		);
//	})

	.factory('AdvlogWebPageServicesSerialize', function($httpParamSerializer,$rootScope) {
		function getUrl(params){
			//http://dvmas002.sirtisistemi.net:10086/stampa_ddt_upload/dump_allegati.cgi?file_s=25022925_20180307144027-0000.tar.gz&file_c=MILANO_LORENTEGGIO_95788_2018.pdf
			var url = $rootScope.config.AdvlogHomePage +'stampa_ddt_upload/ws_stampa_ddt.cgi';
			//injetto la chiave di environment 
			 var serializedParams = $httpParamSerializer(params);
			if (serializedParams.length > 0) {
				url += ((url.indexOf('?') === -1) ? '?' : '&') + serializedParams;
			}
			return url;
		}
		return {getUrl:getUrl};
	})
	
	.factory('DdtDownloadService', function(AdvlogWebPageServicesSerialize, $rootScope,$q,$http) {
		function getStream(params){
        var deferred = $q.defer();

        $http({
            url:AdvlogWebPageServicesSerialize.getUrl(params),
            method:'PUT',//you can use also GET or POST
            data:params,
            withCredentials: true,
            headers:{'Content-type': 'application/pdf'},
            responseType : 'arraybuffer',//THIS IS IMPORTANT
           })
           .success(function (data) {
               console.debug('SUCCESS');
               deferred.resolve(data);
           }).error(function (data) {
                console.error('ERROR');
                deferred.reject(data);
           });

        return deferred.promise;
       }
		return {getStream:getStream};
	})
;
