'use strict';

angular
	.module('gsUiAnomalieInventarioServices', [])
	.factory('printTableMock', function($resource) {
		return $resource('./mock-data/fakedata.json',{ }, {
			getData: {method:'GET', isArray: false}
		});
	})
	/*  INVENTARIO - UI ANOMALIE INVENTARIO - GRID - V_RISOLUZIONE_ANOMALIE  */
	.factory('getListaAnomalie', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrInventario/uiAnomalieInventario/listaAnomalie/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  INVENTARIO - UI ANOMALIE INVENTARIO - GRID - V_FILTRI_ANOMALIE_INVENTARIO  */
	.factory('getFiltriAnomalie', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrInventario/uiAnomalieInventario/filtriAnomalie/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	.factory('azOrdineSpedizioneCambioModalitaSpedizione', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/gestioneSpedizione/action/ordineSpedizione/CambioModalitaSpedizione', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })

;
	


	


    