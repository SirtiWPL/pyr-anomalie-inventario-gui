'use strict';



function gsUiAnomalieInventarioCtrl($rootScope,$q,$scope,$stateParams,$interval,printTableMock,$location,
	$window,$uibModal, $log, $document,getListaAnomalie,$filter,_,gettextCatalog,
	$timeout, filterNumbers, filterDate,ActivityViewServices,getListaMagazziniOperatoreGrid,getFiltriAnomalie){
	
	/* global XLSX */

	$scope.stateParamsUI		= $stateParams;
	$scope.coloreUI 			= $scope.configMainMenu[$scope.stateParamsUI.section].colorB;
	$scope.coloreColUI		= $scope.configMainMenu[$scope.stateParamsUI.section].color;
	$scope.labelUI				= $scope.configMainMenu[$scope.stateParamsUI.section].label;
	$scope.iconUI					= $scope.configMainMenu[$scope.stateParamsUI.section].icon;
	$scope.icon2UI				= $scope.configMainMenu[$scope.stateParamsUI.section].icon2;
	$scope.subLabelUI			= $scope.configMainMenu[$scope.stateParamsUI.section].subLabel;
	
	$scope.buttonFunctionGrid = {
		info			: {icon:'fa fa-info-circle'		,label:'Info Attivita\'', visible: false, classe : 'btn btn-default btn-xs'},
		edita 		: {icon:'fa fa-pencil'				,label:'Risolvi'			, visible: false, ngclick : 'open()', classe : 'btn btn-primary btn-xs'},
		conferma 	: {icon:'fa fa-check'					,label:'Conferma'		, visible: false, classe : 'btn btn-primary btn-xs'},
		vedi 			: {icon:'fa fa-external-link'	,label:'Vedi Attivita'	, visible: false, classe : 'btn btn-info btn-xs'},
		preview 	: {icon:'fa fa fa-eye'				,label:'Preview File Ddt'	, visible: false, classe : 'btn btn-info btn-xs'},
		download 	: {icon:'fa fa fa-download'		,label:'Salva'			, visible: false, classe : 'btn btn-default btn-xs'}
	};
	

	$scope.aAzioniDisponibiliUi = [
		{ value	:	'PRELEVATO',Label	:	'Prelevato'},
		{ value	:	'MATERIALE_RICEVUTO',Label	:	'Materiale Ricevuto'},
		{ value	:	'DICHIARAZIONE_USO_MATERIALE',Label	:	'Dichiarazione Uso Materiale'},
	];
	

	//################### PROFILO AZIONI #########################
	$scope.profiloUI 		= $scope.$parent.profilo;
	
	$scope.showFunction = true;
	
	$scope.filterGruppiIntervento = {};
	$scope.pCallGrid = {};
	$scope.ListHeaderHandling = {};
	$scope.abilitaSearchBT = false;
	
	/* gestione filtri stato e tipo */
	$scope.filtriRadioButtonObj = {
		filtroStato	:	'TUTTI',
		filtroTipo	:	'TUTTI'
	};

	
	if ($scope.profiloUI.admin){
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'ROOT'};
	} else if ($scope.profiloUI.AT) {
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'ASSISTENTE TECNICO POLARYS'};
	} else if ($scope.profiloUI.PM) {
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'PROJECT MANAGER POLARYS'};
	}
	
	/*gestione dei contratti e tipi intervento in base al gruppo operatore*/
	$timeout(function(){

		$scope.caricaMagazziniOperatore('','MULTIPLA');

	},0);
	
	
	$scope.abilitaSearch = function() {
		$scope.data = [];
		console.log('abilitaSearch');
		var contaError = 0;
		$scope.abilitaSearchBT = false;
		if (!$scope.modelMagazziniSelezionati) {
			contaError++;
			//resetto tutti i filtri successivi
			$scope.ListHeaderHandling.dataApertura	= undefined;
			$scope.ListHeaderHandling.dataChiusura	= undefined;
			$scope.ListHeaderHandling.inventario		= undefined;
			
		}
		/*if (!$scope.ListHeaderHandling.dataApertura) {
			contaError++;
			//resetto tutti i filtri successivi
			$scope.ListHeaderHandling.inventario		= undefined;
			
		}
		if (!$scope.ListHeaderHandling.dataChiusura) {
			contaError++;
			//resetto tutti i filtri successivi
			$scope.ListHeaderHandling.inventario		= undefined;
		}*/
		
		if(!$scope.ListHeaderHandling.inventario){
			contaError++;
		}		
		console.log('contaError:',contaError);
		if (contaError === 0){
			$scope.abilitaSearchBT = true;
		} else {
			$scope.abilitaSearchBT = false;
		}
	};
	
	
	
	
	
	/* procedura che permette di avviare la ricerca dei materiali dopo aver selezionato i filtri */
	$scope.cercaMateriali = function(){
		
		$scope.data = [];
		
		console.log('cercaMateriali');
		console.log('Date: ',$scope.ListHeaderHandling);
		console.log('Magazzini: ',$scope.modelMagazziniSelezionati);
		console.log('filtriRadioButtonObj: ',$scope.filtriRadioButtonObj);

		var strDataAp =  $scope.ListHeaderHandling.dataApertura ? ($scope.ListHeaderHandling.dataApertura.getDate() < 10 ? '0'+$scope.ListHeaderHandling.dataApertura.getDate() : $scope.ListHeaderHandling.dataApertura.getDate())+'/'+(($scope.ListHeaderHandling.dataApertura.getMonth() + 1) < 10 ? '0'+($scope.ListHeaderHandling.dataApertura.getMonth() + 1): ($scope.ListHeaderHandling.dataApertura.getMonth() + 1))+'/'+$scope.ListHeaderHandling.dataApertura.getFullYear() : '';
		var strDataCh =  $scope.ListHeaderHandling.dataChiusura ? ($scope.ListHeaderHandling.dataChiusura.getDate() < 10 ? '0'+$scope.ListHeaderHandling.dataChiusura.getDate() : $scope.ListHeaderHandling.dataChiusura.getDate())+'/'+(($scope.ListHeaderHandling.dataChiusura.getMonth() + 1) < 10 ? '0'+($scope.ListHeaderHandling.dataChiusura.getMonth() + 1): ($scope.ListHeaderHandling.dataChiusura.getMonth() + 1))+'/'+$scope.ListHeaderHandling.dataChiusura.getFullYear() : '';
		
		 
		
		$scope.pCallGrid = {};
		
		if ( angular.isUndefined($scope.pCallGrid.filter_MAGAZZINO_INVENTARIO_in) ) {
				//$scope.pCallGrid.filter_MAGAZZINO_INVENTARIO_in = $scope.modelMagazziniSelezionati.CODICE;
				$scope.pCallGrid.filter_MAGAZZINO_INVENTARIO_in = _.pluck($scope.modelMagazziniSelezionati,'CODICE').toString();				
				$scope.pCallGrid.filter_ID_INVENTARIO_contains	= $scope.ListHeaderHandling.inventario.ID_INVENTARIO;
				$scope.pCallGrid.filter_DATA_APERTURA_ge				= strDataAp !== '' ? strDataAp : undefined;
				$scope.pCallGrid.filter_DATA_APERTURA_le				= strDataCh !== '' ? strDataCh : undefined;
				/* gestisco il filtri TIPO */
				if ($scope.filtriRadioButtonObj.filtroTipo === 'TUTTI'){
					$scope.pCallGrid.filter_TIPO_ANOMALIA_in = 'QUARANTENA,ECCEZIONE';
				}
				if ($scope.filtriRadioButtonObj.filtroTipo === 'QUARANTENA'){
					$scope.pCallGrid.filter_TIPO_ANOMALIA_equals = 'QUARANTENA';
				}
				if ($scope.filtriRadioButtonObj.filtroTipo === 'ECCEZIONE'){
					$scope.pCallGrid.filter_TIPO_ANOMALIA_equals = 'ECCEZIONE';
				}
				/* gestisco il filtri TIPO */
				if ($scope.filtriRadioButtonObj.filtroStato === 'TUTTI'){
					$scope.pCallGrid.filter_STATO_RISOLUZIONE_equalsOrNull = 1;
				}
				if ($scope.filtriRadioButtonObj.filtroStato === 'RISOLTO'){
					$scope.pCallGrid.filter_STATO_RISOLUZIONE_equals = 1;
				}
				if ($scope.filtriRadioButtonObj.filtroStato === 'NON RISOLTO'){
					$scope.pCallGrid.filter_STATO_RISOLUZIONE_equalsOrNull = '';
				}
				
		} 
		
		$scope.getGrid();
		
		
	};
	
	
	/* procedura che resetta i filtri */
	$scope.resetFiltriCercaMateriali = function() {
		
		$scope.data 														= [];
		$scope.modelMagazziniSelezionati 				= undefined;
		$scope.ListHeaderHandling.inventario		= undefined;
		$scope.ListHeaderHandling.dataApertura	= undefined;
		$scope.ListHeaderHandling.dataChiusura	= undefined;
		$scope.caricaMagazziniOperatore('','MULTIPLA');
		$scope.abilitaSearch();
		
	};

	
	$scope.magazzinoOperatoreSelected = function($item, $model){
		$scope.abilitaSearch();
	};	
	
	/* procedura che recupera i magazzini associati all'operatore */
	$scope.caricaMagazziniOperatore = function(magazzinoSelezionato,modalita){
		
		$scope.mostraLoaderMagazzini = true;
		$scope.erroreTrovaMagazzino = false;
		
		var filterMagazzino = {};
		if (magazzinoSelezionato === '' ){
			filterMagazzino = {};
		} else {
			filterMagazzino = {

				filter_CODICE_contains	: magazzinoSelezionato,
				sidx										:'CODICE',// ordino per il campo indicato
				sord										: false // asc
				
			};
		}
		if(modalita === 'SINGOLA'){
			return getListaMagazziniOperatoreGrid.getData(filterMagazzino).$promise.then(function(result) {
				console.log(result.data.results);
				if(!result.data.results && magazzinoSelezionato.length >=2){
					$scope.erroreTrovaMagazzino = true;
					$scope.mostraLoaderMagazzini = false;
					return;
				}
				$scope.mostraLoaderMagazzini = false;
				$scope.erroreTrovaMagazzino = false;
				$scope.listaMagazziniOperatore = result.data.results;
				if ($scope.listaMagazziniOperatore.length === 1){
					$scope.modelMagazziniSelezionati = $scope.listaMagazziniOperatore[0];
				} else {
						//$scope.selectAllMagazzini();
						$scope.classMagazziniEdit = 'col-sm-6';
				}
				return result.data.results;
			},function(err) {
				console.log(err);
				$scope.mostraLoaderMagazzini= false;
				$scope.erroreTrovaMagazzino = true;
				$scope.alert.error('Attenzione! riscontrato errore', { ttl: 10000 });
				return;
			});	
		
		} else {
			getListaMagazziniOperatoreGrid.getData({}).$promise.then(function(result) {
					
					console.log(result.data.results);
					
					$scope.listaMagazziniOperatore = result.data.results;
					
					if ($scope.listaMagazziniOperatore.length === 1){
						$scope.modelMagazziniSelezionati = $scope.listaMagazziniOperatore;
						$scope.checkWarehouse = true;
					}

					
					$scope.mostraLoaderMagazzini = false;
			});	
		}
		
		
		
		
		
		

		
	};
	
	/* gestione filtro magazzino */
	$scope.selectAllMag = false;
	$scope.checkWarehouse = false;
	$scope.mostraLoaderMagazzini = false;
	$scope.classMagazziniEdit = 'col-sm-6';
	
	
	
	
	
	
	
	
	$scope.loadMagazzini = function(filtro){
		var localMagaz = $scope.listaMagazziniOperatore;
		$scope.mostraLoaderMagazzini = true;
			$timeout(function(){
				//$scope.caricaMagazziniOperatore('','MULTIPLA');
				$scope.mostraLoaderMagazzini = false;
			},500);
		return $filter('filter')(localMagaz,filtro);
	};
	
	$scope.selectAllMagazzini = function(){
		console.log('selectAllMagazzini',$scope.listaMagazziniOperatore);
		
		$scope.caricaMagazziniOperatore('','MULTIPLA');
		
		var localMagaz = $scope.listaMagazziniOperatore;
		$scope.modelMagazziniSelezionati = undefined;
		$scope.modelMagazziniSelezionati = [];
		$scope.selectAllMag = !$scope.selectAllMag;
		if($scope.selectAllMag){
			$scope.modelMagazziniSelezionati = localMagaz;
		}
		if(!$scope.selectAllMag){
			$scope.modelMagazziniSelezionati = undefined;
		}
		
		//console.log($scope.modelMagazziniSelezionati);
		$scope.abilitaSearch();
	};
	
	
	
	//DatePicker
	$scope.datePicker = {
		dataAperturaStatus: { opened: false },
		dataAperturaOpen: function () {
			$scope.datePicker.dataAperturaStatus.opened = true;
		},
		dataAperturaOptions: {
			maxDate: null,
			showWeeks: true,
			format: 'dd/MM/yyyy'
		},
		dataChiusuraStatus: { opened: false },
		dataChiusuraOpen: function () {
			$scope.datePicker.dataChiusuraStatus.opened = true;
		},
		dataChiusuraOptions: {
			minDate: null,
			maxDate: null,
			showWeeks: true,
			format: 'dd/MM/yyyy'
		},
		//Method for update minDate and MaxDate
		changeMinAndMaxDates: function () {
			if($scope.ListHeaderHandling.dataApertura > $scope.ListHeaderHandling.dataChiusura){
				$scope.ListHeaderHandling.dataChiusura = undefined;
				
			}
			$scope.datePicker.dataChiusuraOptions.minDate = new Date($scope.ListHeaderHandling.dataApertura);
			$scope.abilitaSearch();
		}
	};
	
	
	
	
	
	
	// Lista Inventari
	$scope.getListaInventari = function(inventarioSelez){
		$scope.errorAutocomplete = false;
		$scope.mostraLoaderAutocomplete = true;
		console.log('inventarioSelez:',inventarioSelez);

		/* condizioni modificate per gestire la NON obbligatorieta delle DATE */
		
		var strDataAp =  $scope.ListHeaderHandling.dataApertura ? ($scope.ListHeaderHandling.dataApertura.getDate() < 10 ? '0'+$scope.ListHeaderHandling.dataApertura.getDate() : $scope.ListHeaderHandling.dataApertura.getDate())+'/'+(($scope.ListHeaderHandling.dataApertura.getMonth() + 1) < 10 ? '0'+($scope.ListHeaderHandling.dataApertura.getMonth() + 1): ($scope.ListHeaderHandling.dataApertura.getMonth() + 1))+'/'+$scope.ListHeaderHandling.dataApertura.getFullYear() : '';
		var strDataCh =  $scope.ListHeaderHandling.dataChiusura ? ($scope.ListHeaderHandling.dataChiusura.getDate() < 10 ? '0'+$scope.ListHeaderHandling.dataChiusura.getDate() : $scope.ListHeaderHandling.dataChiusura.getDate())+'/'+(($scope.ListHeaderHandling.dataChiusura.getMonth() + 1) < 10 ? '0'+($scope.ListHeaderHandling.dataChiusura.getMonth() + 1): ($scope.ListHeaderHandling.dataChiusura.getMonth() + 1))+'/'+$scope.ListHeaderHandling.dataChiusura.getFullYear() : '';
		
		return getFiltriAnomalie.getData({
				filter_NOME_INVENTARIO_contains					: (inventarioSelez==='') ? undefined:inventarioSelez,
				filter_MAGAZZINO_INVENTARIO_in				: _.pluck($scope.modelMagazziniSelezionati,'CODICE').toString(),
				filter_DATA_APERTURA_ge								: strDataAp !== '' ? strDataAp : undefined,
				filter_DATA_APERTURA_le								: strDataCh !== '' ? strDataCh : undefined
			}).$promise.then(function(result) {
			console.log(result.data.results);
			if(!result.data.results && inventarioSelez && inventarioSelez.length >=2){
				$scope.errorAutocomplete = true;
				$scope.mostraLoaderAutocomplete = false;
				return;
			}
			$scope.mostraLoaderAutocomplete = false;
			return result.data.results;
		},function(err) {
			console.log(err);
			$scope.mostraLoaderAutocomplete = false;
			$scope.alert.error('Attenzione! riscontrato errore', { ttl: 10000 });
			return;
		});	
	};
	
	
	$scope.inventarioSelected = function($item, $model){
		console.log('inventarioSelected',$item);
		console.log('inventarioSelected',$model);
		$scope.abilitaSearch();
		
	};

	
	/* gestione mostra nascondi colonne grid */
	$scope.selectAllColonneGrid = function(option) {
		_.each($scope.configGridUI, function(value,key) {
			
			if (value.enabled && key !== 'FUNCTION'){
				
				value.visible = option==='TUTTI' ? true:false;
			}
		});
	};		

	$scope.configGridUI		= {
		FUNCTION: {
		    divider: false,
		    name: 'FUNCTION',
		    label: '',
		    visible: true,
		    orderable: false,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: 'fa-cogs',
		    enabled: false,
				enableFilter : false,
		    type:'string'
		  },
		  MAGAZZINO_INVENTARIO: {
		    divider: true,
		    name: 'MAGAZZINO_INVENTARIO',
		    label: 'MAGAZZINO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '110px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  ID_INVENTARIO: {
		    divider: false,
		    name: 'ID_INVENTARIO',
		    label: 'INVENTARIO',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  DATA_APERTURA: {
		    divider: false,
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'date'
		  },
		  TIPO_ANOMALIA: {
		    divider: false,
		    name: 'TIPO_ANOMALIA',
		    label: 'TIPO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '110px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  STATO_RISOLUZIONE: {
		    divider: false,
		    name: 'STATO_RISOLUZIONE',
		    label: 'STATO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  DESCRIZIONE_ANOMALIA: {
		    divider: false,
		    name: 'DESCRIZIONE_ANOMALIA',
		    label: 'DESC. ANOMALIA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '230px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  ID_SIRTI: {
		    divider: false,
		    name: 'ID_SIRTI',
		    label: 'ID SIRTI',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  CODICE_SIRTI: {
		    divider: false,
		    name: 'CODICE_SIRTI',
		    label: 'CODICE SIRTI',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  PART_NUMBER: {
		    divider: false,
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  SERIAL_NUMBER: {
		    divider: true,
		    name: 'SERIAL_NUMBER',
		    label: 'SERIAL NUMBER',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  }



	};


	/* configurazione della modale invocata dalla UI */
	$scope.configModalUI =	{
		DESCRIZIONE_ANOMALIA: {
			divider : false,
			id : 'DESCRIZIONE_ANOMALIA',
			name : 'DESCRIZIONE_ANOMALIA',
			label : 'DESCRIZIONE ANOMALIA',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '220px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'textarea',
			ngClass : 'col-md-12'
		},
		/*MAGAZZINO_INVENTARIO: {
			divider : false,
			id : 'MAGAZZINO_INVENTARIO',
			name : 'MAGAZZINO_INVENTARIO',
			label : 'MAGAZZINO',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '220px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'string',
			ngClass : 'col-md-6'
		},*/
		ID_SIRTI : {
			divider : false,
			id : 'ID_SIRTI',
			name : 'ID_SIRTI',
			label : 'ID SIRTI',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '100px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'number',
			ngClass : 'col-md-6'
		},
		CODICE_SIRTI : {
			divider : false,
			id : 'CODICE_SIRTI',
			name : 'CODICE_SIRTI',
			label : 'CODICE SIRTI',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '100px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'string',
			ngClass : 'col-md-6'
		},					
		PART_NUMBER: {
			divider : false,
			id : 'PART_NUMBER',
			name : 'PART_NUMBER',
			label : 'PART NUMBER',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '220px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'string',
			ngClass : 'col-md-6'
		},
		SERIAL_NUMBER: {
			divider : false,
			id : 'SERIAL_NUMBER',
			name : 'SERIAL_NUMBER',
			label : 'SERIAL NUMBER',
			visible : true,
			orderable : true,
			filtered : false,
			color : $scope.configMainMenu[$scope.stateParamsUI.section].color,
			width : '220px',
			icon : '',
			enabled : false,
			enableFilter : true,
			type:'string',
			ngClass : 'col-md-6'
		}
	
	};

	$scope.options = {
			orderBy : ['ID_INVENTARIO'],
			global : '',
			searchSingleColumn : true
	};
	$scope.currentPage  = 1;
	$scope.itemsPerPage = 10;
	$scope.orderByField = $scope.configGridUI[$scope.options.orderBy[0]].name;
	$scope.reverseSort  = false;
	$scope.maxSizePager = 6;
	$scope.nuovoNumero = 10;
	$scope.mostraLoaderGrid = false;
	
	// composto data header e data
	$scope.objectDati = {
			header:$scope.configGridUI,
			data:[]
	};
	
	
	//##################################### ATTIVAZIONE FILTRI ###################################//
	$scope.filterText = {}; // mappa chiave/valore con campo/valore da filtrare
	$scope.filterOperator = {}; // mappa chiave/valore con campo/operatore di filtro da applicare
	$scope.sortDirection = {}; // mappa chiave/valore con campo/direzione di ordinamento
	$scope.sortOrderArray = []; // array con l'elenco ordinato dei campi da ordinare
	$scope.sortOrderMap = {}; // mappa chiave/valore con i campi/posizione da ordinare. parallelo a $scope.sortOrderArray ma serve per essere passato
								// alla direttiva sirti-column-sort-and-filter nell'attributo sort-position in modo che non debba essere calcolato
	
	$scope.filterOrOrderApplied = false;
	
	function isFilterOrOrderApplied() {
		var isFiltered = false;
		var isSorted = false;
		_.each($scope.filterOperator, function(value) {
			if(!_.isUndefined(value)) {
				isFiltered = true;
			}
		});
		_.each($scope.sortOrderMap, function(value) {
			if(!_.isUndefined(value)) {
				isSorted = true;
			}
		});
		return isFiltered || isSorted;
	}
	
	$scope.$watch('filterOperator', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	$scope.$watch('sortOrderMap', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	// funzione che recepisce l'applica della direttiva sirti-column-sort-and-filter
	$scope.applyFilterAndSort = function(field, filterText, filterOperator, sortDirection) {
		if(field === 'id' && filterText && !filterText.match(/^\d+$/)) {
			$scope.alert.warning(gettextCatalog.getString('Activity Id must be a number'));
			return;
		}
	
		$scope.filterText[field] = filterText;
		$scope.filterOperator[field] = filterOperator;
		$scope.sortDirection[field] = sortDirection;
		if($scope.sortDirection[field]) {
			// la direzione di sort del campo è valorizzata e la aggiungo all'array se non ancora presente
			if(!_.contains($scope.sortOrderArray, field)) {
				$scope.sortOrderArray.push(field);
			}
		} else {
			// ...altrimenti elimino il campo dall'ordinamento
			$scope.sortOrderArray = _.without($scope.sortOrderArray, field);
		}
		// costruisco la mappa a partire dall'array di ordinamento (vedi nota sull'oggetto $scope.sortOrderMap)
		$scope.sortOrderMap = {};
		var i = 1;
		_.each($scope.sortOrderArray, function(value) {
			$scope.sortOrderMap[value] = i++;
		});
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	
	// pulisco filtri
	$scope.clearFiltersAndOrdering = function() {
		$scope.filterText = {};
		$scope.filterOperator = {};
		$scope.sortDirection = {};
		$scope.sortOrderArray = [];
		$scope.sortOrderMap = {};
		$scope.options.global = '';
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	//###################################### FINE PREPARAZIONE RICERCA #####################################//
	
	$scope.getGrid = function() {
		//var withFilter = [];//['permitsAreaId'];
		//var withOutSorter = [];
		//var withAutocompleate = [];//['macroTaskSubCategory','macroTaskCategory'];
		//var hidden = [];//['Pfp','Pop','projectId','customerId','contractId','buildingId','macroTaskType'];
		var oldTemp = $scope.data || [];
		var temp = [];
		
		$scope.mostraLoaderGrid = true;
	

	
			// #################### DEFAULT IN BASE PROFILO AZIONI ###################
		var pCall = $scope.pCallGrid;
		// #################### DEFAULT IN BASE PROFILO AZIONI  FINE###################

		// parametri in URI di ricerca su BKEND		
		var params = {
				sort:[],
				pCall : pCall
		};
		
		//################################# POPOLAMENTO FILTRI ##################################//
		
		var localFilter = {};
		// definisco i parametri di filtro (testo e operatore)
		_.each($scope.filterOperator, function(value, key) {
			if($scope.filterOperator[key] && $scope.filterText[key]) {
				// solo in questo caso setto il parametro dello skip a 0 
				// in modo da avere sempre la ripartenza dallo 0esimo
				//params.skip = 0;
				params[key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				// popolo i filtri locali
				localFilter[key] = $scope.filterText[key];

				// remap per far funzionare l'equal
				if($scope.filterOperator[key] === 'equal'){$scope.filterOperator[key] = 'equals';}
				// aggiungo i parametri di ricerca a pCall
				pCall['filter_'+ key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				
				if(key === 'status') {
					// per lo status sostituisco gli spazi con _ secondo la convenzione di chiamare gli stati senza spazi e con le parole divise da _
					params[key + '_' + $scope.filterOperator[key]] = params[key + '_' + $scope.filterOperator[key]].replace(/ /g, '_');
				}
			}
		});
	
		// definisco l'ordine di sort
		_.each($scope.sortOrderArray, function(key) {
			if($scope.sortDirection[key]) {
				params.sort.push(($scope.sortDirection[key] === 'ASC' ? '' : '-') + key);
			}
		});
	
		// se non c'è alcun ordinamento ordino per id DESC
		// agisco in questo punto per evitare che la direttiva sirti-column-sort-and-filter evidenzi tale ordinamento
		if(params.sort.length === 0) {	
			params.sort = $scope.options.filterBy;
		}
		// ############################################ FINE POPOLAMENTO ########################################// 
		getListaAnomalie.getData(pCall).$promise.then(function(result) {
			temp = result.data.results;
			$scope.totalItems = result.data.count;
			
			$scope.objectDati.data = temp; 
			$scope.objectDati = filterNumbers(filterDate($scope.objectDati));

			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			// FIXME filtri locali in caso attivati i filtri sui campi 
			$scope.objectDati.data = $filter('orderBy')($scope.objectDati.data = $filter('filter')((temp || oldTemp),localFilter),params.sort);
			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			$scope.data = $scope.objectDati.data;
			
			//console.log($scope.data);
			
			$scope.data = _.each($scope.objectDati.data,function(row){

				row.hiddenButton = false;
				row.hiddenViewActivity = false;
				
				/* rimappo STATO_RISOLUZIONE con gli stessi valori del filtro - Inizio */
				
				if ((row.STATO_RISOLUZIONE === 1)||(row.STATO_RISOLUZIONE === '1')){
					row.STATO_RISOLUZIONE = 'RISOLTO';
				} else {
					row.STATO_RISOLUZIONE = 'NON RISOLTO';
				}
				
				/* rimappo STATO_RISOLUZIONE con gli stessi valori del filtro - Fine */
				
				/* gestione dei pulsanti operativi - Inizio */
				
				/*caso GRIGIO e VERDE: nessun pulsante da visualizzare */
				
				if ((row.SEMAFORO === 'GRIGIO')||(row.SEMAFORO === 'VERDE')) {
					row.showRisolviBtn = false;
				}
				
				/*caso ROSSO: nessun pulsante da visualizzare */
				
				if (row.SEMAFORO === 'ROSSO') {
					row.showRisolviBtn = true;
				}
				
				/*caso GIALLO: nessun pulsante da visualizzare */
				
				if (row.SEMAFORO === 'GIALLO') {
					row.showRisolviBtn = true;
				}
				
				/*caso BLU: nessun pulsante da visualizzare */
				
				if (row.SEMAFORO === 'BLU') {
					row.showRisolviBtn = true;
				}
				
				/* gestione dei pulsanti operativi - Fine */

				
			});
			
			$scope.mostraLoaderGrid = false;
			

				
		},
		function(err) {
			$scope.mostraLoaderGrid = false;
			$scope.alert.error(err.data);
		});
		
	
		
	};
	
	
	
	
		// funzione di export tabella
$scope.exportToExcel = function(){
	
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'ANOMALIE_INVENTARIO';
	
	nomeFile	= $scope.configMainMenu[$scope.stateParamsUI.section].name;
	
	angular.forEach($scope.configGridUI, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='FUNCTION') && value.visible === true){
			confHeader[key] = {name : value.name, id : value.name, label : value.label, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.data;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.mostraLoaderUiModal = false;
		
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers

};	
	
	
	/* cambia pagina */ 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * $scope.itemsPerPage;
		return startPos;
	};
	
	/*$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  ActivityViewServices.getUrl({
			'stile':'custom_rma_sirti',
			'id_attivita_passata':item.ID_ATTIVITA
		});
		$window.open(urlMappa,'_blank','location=no');
	};*/
	
	$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  function(){
			var urlPolaris = ActivityViewServices.getUrl({
				'stile':'custom_rma_sirti',//FIXME dedicare un classe lato polaris
				'id_attivita_passata':item.ID_ATTIVITA
			});
		
			return urlPolaris;
		};
		
		//$window.open(urlMappa,'_blank','location=no');
		var urlActivity = {src: urlMappa(), title:'ACTIVITY ID : '+item.ID_ATTIVITA+' '};
	
		/* UI MODALE - Inizio */
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,urlActivity) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/activityViewModal.html',
				controller: 'ActivityViewModalCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () { return itemSelected; },
						uiChiamante: function () { return uiChiamante; },
						uiConfig: function () { return uiConfig; },
						tab: function () { return tab; },
						givenUrl:function(){return urlActivity;}
				}
			});
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();
				
			});
		};
		$ctrl.open('fullscreen',null,item, $scope.configMainMenu[$scope.stateParamsUI.section] ,$scope.configModalUI,'',urlActivity);
	};
	
	
	/* UI MODALE AZIONE - Inizio */	
	
	$scope.apriModaleAzione = function (itemSelected){
		

		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/

			
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/uiAnomalieInventario/modalAzioneUI.html',
				controller: 'modalAzioneCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () {
							return itemSelected;
						},
						uiChiamante: function () {
							return uiChiamante;
						},
						uiConfig: function () {
							return uiConfig;
						}
				}
			});
		
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();

			});
		};
		
		
		$ctrl.open('',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI);
	};
	
	/* UI MODALE INFO - Inizio */
	
	
	$scope.apriModaleInfo = function (itemSelected,objInfoButtonModalSelected,keySelected){
		

		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,objInfoButtonModalSelected,keySelected) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/

			
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/uiAnomalieInventario/modalInfoUI.html',
				controller: 'modalInfoCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () {
							return itemSelected;
						},
						uiChiamante: function () {
							return uiChiamante;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						},
						objInfoButtonModalSelected: function () {
							return objInfoButtonModalSelected;
						},
						keySelected: function () {
							return keySelected;
						}
				}
			});
		
			modalInstance.result.then(function (objRet) {
				//selected = selectedItem;
				console.log('objRet: ',objRet);

				$scope.apriModaleAzione(objRet.item,objRet.azioneAttivita,objRet.key);

			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
								
				$scope.getGrid();

			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI,'',objInfoButtonModalSelected,keySelected);
	};	

}


angular.module('guiPyrAnomalieInventario').controller('gsUiAnomalieInventarioCtrl', gsUiAnomalieInventarioCtrl);