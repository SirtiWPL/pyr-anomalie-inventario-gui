
'use strict';

/* Controller della Form Modale che verra' chiamata da tutte le UI */

function modalInfoCtrl($scope,$rootScope,$uibModalInstance,itemSelected,uiChiamante,uiConfig,tab,objInfoButtonModalSelected,keySelected,
Alert,$window,_ , $timeout,filterDate,filterNumbers,getTaOrdineSpedizioneInfoGrid,getTaStaffettaInfoGrid,getTaDicUsoInforGrid,
ActivityViewServices,$uibModal, $log){
	/*jshint validthis: true */
	/* global XLSX */
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.$ctrl 	= this;
	var oriCtrl		={};
	
	$scope.$ctrl.alert											= Alert;
	$scope.$ctrl.mostraLoaderUiModal				= true;
	$scope.$ctrl.mostraLoaderAutocomplete		= false;
	$scope.$ctrl.collapseSummary						= false;
	$scope.$ctrl.showInfoErrorGrid 					= false;

	$scope.$ctrl.item									= itemSelected;
	$scope.$ctrl.chiamante						= uiChiamante;
	$scope.$ctrl.config								= uiConfig;
	$scope.$ctrl.cssSmall							= 'small';
	$scope.$ctrl.tab									= tab;
	$scope.$ctrl.objInfoButtonModal 	= objInfoButtonModalSelected;
	$scope.$ctrl.colonnaOperativa			= keySelected;
	$scope.$ctrl.disableCbByActionSel = true;
	
	$scope.$ctrl.disabledConferma	=	true;

	$scope.$ctrl.config.STATO.visibleCol = true;
	$scope.$ctrl.config.DICHIARAZIONE_USO.visibleCol = false;
	
	
	
	$scope.$ctrl.disabilitaEditCustom = {

	};
	
	
	console.log('item',$scope.$ctrl.item);
	console.log('objInfoButtonModal',$scope.$ctrl.objInfoButtonModal);
	console.log('colonnaOperativa',$scope.$ctrl.colonnaOperativa);
	
	console.log('chiamante',$scope.$ctrl.chiamante);
	console.log('config',$scope.$ctrl.config);

	$scope.$ctrl.selectAll = false;
	$scope.$ctrl.activitiesSelected = [];

	// checked All activities
	$scope.$ctrl.checkedAll = function(){
		// ckeck di tutti gli elementi
		console.log('qui');
		_.each( _.where( $scope.$ctrl.listaAttivitaMassive) , function(att){
			att.SELECTED = $scope.$ctrl.selectAll;
		});
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		checkElement();
	};
	
	// funzione PRIVATA di check degli elementi
	var checkElement = function(){
		$scope.$ctrl.activitiesSelected = [];
		$scope.$ctrl.disabledConferma = true;
		_.each( _.where($scope.$ctrl.listaAttivitaMassive , { SELECTED : true } ) , function(att){
			$scope.$ctrl.activitiesSelected.push(att);
		});
		// attiva il options.selectAll
		if( $scope.$ctrl.activitiesSelected.length > 0 ){ $scope.$ctrl.selectAll = true; $scope.$ctrl.disabledConferma = false;}
	};

	// aggiunge su un array temporaneo
	$scope.$ctrl.addActivitiesSelected = function(){
		checkElement();
		if(angular.isDefined($scope.$ctrl.activitiesSelected)){
			if($scope.$ctrl.activitiesSelected.length === $scope.$ctrl.listaAttivitaMassive.length){
				$scope.$ctrl.selectAll = true;
				$scope.$ctrl.disabledConferma = false;
			}
		}
		
	};

	
	
	/* in base alla CFG del chiamante  determino come popolare la griglia delle attivita selezionate */


	$scope.$ctrl.getGridMassiva = function() {
		
		$scope.$ctrl.selectAll = false;
		
		
		/* TA_ORDINE_SPEDIZIONE */
		
		if ($scope.$ctrl.objInfoButtonModal.tabella === 'TA_ORDINE_SPEDIZIONE'){
			
			var filterGridTaOrdSped = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			
			/* solo per STATO e ID_ATTIVITA_MASTER */
			filterGridTaOrdSped = {
				filter_STATO_in										: $scope.$ctrl.objInfoButtonModal.filterTabella.STATO,
				filter_ID_ATTIVITA_MASTER_equals	: $scope.$ctrl.item.ID_ATTIVITA,
			};
		
		
			getTaOrdineSpedizioneInfoGrid.getData(filterGridTaOrdSped).$promise.then(function(result) {
					
					console.log(result.data.results);
				
					/* ciclo i valori trovati e ingetto il selectBox */
				
					angular.forEach(result.data.results, function(act) {
					
						console.log(act);
						act.SELECTED = false;
						
						if ((!act.SERIAL_NUMBER) || (act.SERIAL_NUMBER === '') || (act.SERIAL_NUMBER === null) || (act.SERIAL_NUMBER === undefined)){
							act.CLASSIFICAZIONE_MATERIALE = 'CONSUMABILE';
						} else {
							act.CLASSIFICAZIONE_MATERIALE = 'NON CONSUMABILE';
						}						
						
						
					});
					
					console.log(result.data.results);
				
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(result.data.results,'SELECTED').reverse();
				
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_ORDINE_SPEDIZIONE */
		
		/* TA_STAFFETTA */
		
		else if ($scope.$ctrl.objInfoButtonModal.tabella === 'TA_STAFFETTA'){
			
			var filterGridTaStaffetta = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			
			/* solo per STATO e ID_ATTIVITA_MASTER */
			filterGridTaStaffetta = {
				filter_STATO_in										: $scope.$ctrl.objInfoButtonModal.filterTabella.STATO,
				filter_ID_ATTIVITA_MASTER_equals	: $scope.$ctrl.item.ID_ATTIVITA,
			};
		
		
			getTaStaffettaInfoGrid.getData(filterGridTaStaffetta).$promise.then(function(result) {
					
					console.log(result.data.results);
				
					/* ciclo i valori trovati e ingetto il selectBox */
				
					angular.forEach(result.data.results, function(act) {
					
						console.log(act);
						act.SELECTED = false;
						
						if ((!act.SERIAL_NUMBER) || (act.SERIAL_NUMBER === '') || (act.SERIAL_NUMBER === null) || (act.SERIAL_NUMBER === undefined)){
							act.CLASSIFICAZIONE_MATERIALE = 'CONSUMABILE';
						} else {
							act.CLASSIFICAZIONE_MATERIALE = 'NON CONSUMABILE';
						}
				
					});
					
					console.log(result.data.results);
				
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(result.data.results,'SELECTED').reverse();
				
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_STAFFETTA */
		
		/* TA_DICHIARAZIONE_USO_MATERIALE */
		
		else if ($scope.$ctrl.objInfoButtonModal.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
			
			var filterGridTaDicUso = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			$scope.$ctrl.config.DICHIARAZIONE_USO.visibleCol = false;
			
			/* solo per STATO e ID_ATTIVITA_MASTER */
			filterGridTaDicUso = {
				filter_STATO_in											: $scope.$ctrl.objInfoButtonModal.filterTabella.STATO,
				filter_ID_ATTIVITA_MASTER_equals		: $scope.$ctrl.item.ID_ATTIVITA,
			};
		
		
			getTaDicUsoInforGrid.getData(filterGridTaDicUso).$promise.then(function(result) {
					
					console.log(result.data.results);
				
					/* ciclo i valori trovati e ingetto il selectBox */
				
					angular.forEach(result.data.results, function(act) {
					
						console.log(act);
						act.SELECTED = false;
					});
					
					console.log(result.data.results);
				
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(result.data.results,'SELECTED').reverse();
				
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_DICHIARAZIONE_USO_MATERIALE */
		
	};

	

	$scope.$ctrl.hasError = {};
	
	
	
	$scope.$ctrl.myColSpan = {
		PART_NUMBER : 'col-md-12 col-xs12',
	};

	
	/* ripristina modale */

	
	$scope.$ctrl.ripristinaModale = function (){
	
		
		/* resetto la sezione verify con tutti i campi, in base all'azione chiamante */
		console.log('muori');
		
		$scope.$ctrl = angular.copy(oriCtrl);
		$scope.fModalInfo.$setPristine();
	
				
	};
	


  $scope.$ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
	
	$scope.$ctrl.apriModaleAzione = function (item,azioneAttivita,key) {
		var objRet =	{
										item						: item,
										azioneAttivita	: azioneAttivita,
										key							:	key
									};
    $uibModalInstance.close(objRet);
		//apriModaleAzione(item,azioneAttivita,key);
  };


	/* */
	
	$scope.$ctrl.clickConferma = function () {
		

		
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		
		$scope.$ctrl.hasErrorAct 		= {};
		$scope.$ctrl.hasErrorEscAct 	= {};
		$scope.$ctrl.hasErrorIcon 		= {};
		$scope.$ctrl.hasSuccessEscAct	= {};
		
		$scope.$ctrl.mostraLoaderUiModal = true;
	
		console.log($scope.$ctrl.actionSelected);
		console.log($scope.$ctrl.chiamante.name);
		var datiCampilati =	{};
		var prosegui = false;	
		

	};
	

	/* paginazione dei risultati */
	
	$scope.$ctrl.currentPage  = 1;
	$scope.$ctrl.itemsPerPage = 10;
	$scope.$ctrl.orderByField = 'ID_ATTIVITA';
	$scope.$ctrl.reverseSort  = false;
	$scope.$ctrl.maxSizePager = 5;
	$scope.$ctrl.pager = [2, 5, 10, 20, 50,75,100];
	
	/* cambia pagina */ 
	$scope.$ctrl.pageChanged = function() {
		var startPos = ($scope.$ctrl.currentPage - 1) * $scope.$ctrl.itemsPerPage;
		return startPos;
	};
	
	/* gestione della scrollbar-y della grid */
	$scope.$watch('$ctrl.itemsPerPage', function(pg) {
		$timeout(function() {
			console.log('offsetWidth: ',angular.element('#tBodyGridModaleSped').prop('offsetWidth'));
			console.log('scrollWidth: ',angular.element('#tBodyGridModaleSped').get(0).scrollWidth);
			var	offsetWidth	=	angular.element('#tBodyGridModaleSped').prop('offsetWidth');
			var	scrollWidth	=	angular.element('#tBodyGridModaleSped').get(0).scrollWidth;
			if(scrollWidth === offsetWidth){
				angular.element('#tHeadGridModaleSped').css('min-width', offsetWidth);
				angular.element('#tHeadGridModaleSped').css('max-width', offsetWidth);
				angular.element('#tHeadGridModaleSped').css('width', offsetWidth);
			} else {
				angular.element('#tHeadGridModaleSped').css('min-width', scrollWidth);
				angular.element('#tHeadGridModaleSped').css('max-width', scrollWidth);
				angular.element('#tHeadGridModaleSped').css('width', scrollWidth);
			}
		});
	});	
	
	
	
// funzione di export tabella
$scope.$ctrl.exportToExcel = function(){
	
	
	
	$scope.$ctrl.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'ATTIVITA';
	
	nomeFile	= $scope.$ctrl.chiamante.name;
	
	angular.forEach($scope.$ctrl.config, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='SELECTED') && value.visibleInAction === false){
			confHeader[key] = {name : value.name, id : value.name, label : value.label, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.$ctrl.listaAttivitaMassive;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.$ctrl.mostraLoaderUiModal = false;

};

	/* gestione mostra nascondi colonne grid */
	$scope.$ctrl.selectAllColonneGrid = function(option) {
		_.each($scope.$ctrl.config, function(value,key) {
			
			if (value.mostraNascondi && key !== 'FUNCTION'){
				
				value.visibleCol = option==='TUTTI' ? true:false;
			}
		});
	};
	
	$timeout(function() {
		$scope.$ctrl.getGridMassiva();
	},400);
	
	// pulisco filtri
	$scope.$ctrl.clearFiltersAndOrdering = function() {
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		// invoco il reload della tabella
		$scope.$ctrl.getGridMassiva();
	};
	
	
	
	$scope.$ctrl.viewActivity = function(item,key){
		// restituisce l'URL dinamico creato da
		var urlMappa =  function(){
			var urlPolaris = ActivityViewServices.getUrl({
				'stile':'custom_rma_sirti',//FIXME dedicare un classe lato polaris
				'id_attivita_passata':item[key]
			});
		
			return urlPolaris;
		};
		
		//$window.open(urlMappa,'_blank','location=no');
		var urlActivity = {src: urlMappa(), title:'ACTIVITY ID : '+item[key]+' '};
	
		/* UI MODALE - Inizio */
		var $ctrlVA = this;
		
		$ctrlVA.animationsEnabled = true;
		$ctrlVA.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,urlActivity) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrlVA.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/activityViewModal.html',
				controller: 'ActivityViewModalCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () { return itemSelected; },
						uiChiamante: function () { return uiChiamante; },
						uiConfig: function () { return uiConfig; },
						tab: function () { return tab; },
						givenUrl:function(){return urlActivity;}
				}
			});
			modalInstance.result.then(function (selectedItem) {
				$log.info('selectedItem:',selectedItem);
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				
			});
		};
		$ctrlVA.open('fullscreen',null,item, $scope.$ctrl.chiamante ,$scope.$ctrl.config,'',urlActivity);
	};
		
	
	
// jscs:enable requireCamelCaseOrUpperCaseIdentifiers	
}

angular.module('guiPyrAnomalieInventario').controller('modalInfoCtrl', modalInfoCtrl);