
'use strict';

/* Controller della Form Modale che verra' chiamata da tutte le UI */

function modalAzioneCtrl($scope,$rootScope,$uibModalInstance,itemSelected,uiChiamante,uiConfig,
Alert,$window,_ , $timeout,filterDate,filterNumbers,azRisolviAnomaliaPostInv,$log){
	/*jshint validthis: true */
	/* global XLSX */
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.$ctrl 	= this;
	var oriCtrl		={};
	
	$scope.$ctrl.alert											= Alert;
	$scope.$ctrl.mostraLoaderUiModal				= false;
	$scope.$ctrl.mostraLoaderAutocomplete		= false;
	$scope.$ctrl.collapseSummary						= false;
	$scope.$ctrl.showInfoErrorGrid 					= false;

	$scope.$ctrl.item							= itemSelected;
	$scope.$ctrl.chiamante				= uiChiamante;
	$scope.$ctrl.config						= uiConfig;
	$scope.$ctrl.cssSmall					= 'small';

	
	
	$scope.$ctrl.disabledConferma	=	true;
	$scope.$ctrl.nascondiConferma = false;

	/* gestione del pulsante conferma a seonda del SEMAFORO */
	if ($scope.$ctrl.item.SEMAFORO === 'ROSSO') {
		/* note obbligatorie disabilito conferma */
		$scope.$ctrl.disabledConferma	=	true;
		$scope.$ctrl.chiamante.color='bg-red color-palette';
	}
	
	/*caso GIALLO: nessun pulsante da visualizzare */
	
	if ($scope.$ctrl.item.SEMAFORO === 'GIALLO') {
		/* note obbligatorie disabilito conferma */
		$scope.$ctrl.disabledConferma	=	true;
		$scope.$ctrl.chiamante.color='bg-yellow color-palette';
	}
	
	/*caso BLU: nessun pulsante da visualizzare */
	
	if ($scope.$ctrl.item.SEMAFORO === 'BLU') {
		/* note pre-compilate abilito conferma */
		$scope.$ctrl.disabledConferma	=	false;
		$scope.$ctrl.chiamante.color='bg-aqua color-palette';
		$scope.$ctrl.item.NOTE_RISOLUZIONE = $scope.$ctrl.item.DESCRIZIONE_SEMAFORO;
	}
	

	$scope.$ctrl.hasError = {};
	
	
	
	$scope.$ctrl.myColSpan = {
		PART_NUMBER : 'col-md-12 col-xs12',
	};

	
	/* ripristina modale */

	
	$scope.$ctrl.ripristinaModale = function (){
	
		
		/* resetto la sezione verify con tutti i campi, in base all'azione chiamante */
		console.log('muori');
		
		$scope.$ctrl = angular.copy(oriCtrl);
		$scope.fModalAzione.$setPristine();
	
				
	};
	


  $scope.$ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };



/* procedura che gestisce l'azione selezionata */

$scope.$ctrl.clickConferma = function(){
		
		console.log('clickConferma');
		

		var paramInsertBuffer					= {};

		
		$scope.$ctrl.mostraLoaderUiModal = true;
		

			/* cotruisco i parametri da inviare alla tabella di buffer */
			
		paramInsertBuffer = {
			ID_SIRTI						:	$scope.$ctrl.item.ID_SIRTI,
			ID_INVENTARIO				:	$scope.$ctrl.item.ID_INVENTARIO,
			NOTE_RISOLUZIONE		:	$scope.$ctrl.item.NOTE_RISOLUZIONE
		};

		console.log('paramInsertBuffer: ',paramInsertBuffer);
		
		console.log('RisolviAnomaliaPostInv');
		
		azRisolviAnomaliaPostInv.getData(paramInsertBuffer).$promise.then(function(resultAz) {
		
			console.log(resultAz);
			
			$scope.$ctrl.mostraLoaderUiModal = false;
			
			if (resultAz.data.ESITO === 'OK'){
				$scope.$ctrl.alert.success('Risoluzione avvenuta con successo', { ttl: 10000 });
				//$scope.$ctrl.cancel();
				$scope.$ctrl.nascondiConferma=true;
			}
			else {
				$scope.$ctrl.alert.error('Risoluzione Fallita a causa: '+resultAz.DESC_ESITO, { ttl: 10000 });
			}
			
			
		});	


};




// jscs:enable requireCamelCaseOrUpperCaseIdentifiers	
}

angular.module('guiPyrAnomalieInventario').controller('modalAzioneCtrl', modalAzioneCtrl);