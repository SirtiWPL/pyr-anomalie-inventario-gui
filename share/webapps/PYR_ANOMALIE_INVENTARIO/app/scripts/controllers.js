'use strict';


function guiPyrAnomalieInventarioCtrl($state,$stateParams,$rootScope,$q,$scope,$interval,printTableMock,$location,$window,
	$uibModal, $log, $document,Alert,UserProfile,UiConfig,_,$filter,$timeout,getListaAnomalie){
	
	$scope.alert = Alert;
	$scope.firtTest = 'Primo Test';
	$scope.classCollapse = 'sidebar-collapse';
	$scope.classMenuOpen = 'menu-open';
	$scope.propDisplay   = 'block';
	$scope.stateParams = $stateParams;
	$scope.keyIn = $scope.stateParams.key || 'MAIN';
	$scope.linkLogOut = UiConfig.AdvlogHomePage() + 'principale.html';
	/*$scope.profilo = {
			PM:AdcProfile.isProjectManager(),
			CM:AdcProfile.isCapoMagazziniere(),
			MG:AdcProfile.isMagazziniere(),
			admin:AdcProfile.isAdmin(),
			magazzini:AdcProfile.magazzini(),
			quadruple:AdcProfile.progetti(),
			nomeOperatore:UserProfile.nomeUtente()
	};*/
	
	/* Inizio gestione nuova profilo senza ADC */
	
	$scope.isAbilitatoGui = false;
	
	$scope.getProfiloGUI = function(){
		
			console.log('getProfiloGUI');

			var arrSup = $filter('filter')($scope.profilo.gruppi, 'INVENTARIO_ADM');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.CM = true;
			} else {
				$scope.profilo.CM = false;
			}

			arrSup = $filter('filter')($scope.profilo.gruppi, 'MAGAZZINIERE');
			console.log(arrSup);
			if (arrSup.length > 0) {
				$scope.profilo.MG = true;
			} else {
				$scope.profilo.MG = false;
			}
	};
	
	$scope.profilo = {
		PM:null,
		CM:null,
		MG:null,
		SQ:null,
		AT:null,
		admin:UserProfile.isAdmin(),
		magazzini:null,
		quadruple:null,
		nomeOperatore:UserProfile.nomeUtente(),
		gruppi: UserProfile.gruppi()
	};

	console.log('$scope.profilo');
	console.log($scope.profilo);
	
	/* fine gestione nuova profilo senza ADC */
	
	$scope.tempoRinnovoSessione	= 900000; //60000,900000


	$scope.collapseMenu = function(){
		if ($scope.classCollapse === 'sidebar-collapse'){
			$scope.classCollapse = ''; 
		}
		else{
			$scope.classCollapse = 'sidebar-collapse';
		}
	};

	$scope.openTreeView = function(){
		if ($scope.classMenuOpen === 'menu-open'){
			$scope.classMenuOpen = '';
			$scope.propDisplay   = 'none';
		}else{
			$scope.classMenuOpen = 'menu-open';
			$scope.propDisplay   = 'block';
		}
	};

	$scope.loginOperatore = '';


	// gestione rinnovo sessione
	$interval(function () {
		
		console.log('Rinnovo la sessione dopo '+$scope.tempoRinnovoSessione+' min');
		
		getListaAnomalie.getData({}).$promise.then(function(result) {
			console.log('getListaAnomalie',result);
		});



	}, $scope.tempoRinnovoSessione);	
	


	// config Main
	$scope.configMainMenu = {
			UI_ANOMALIE_INVENTARIO : {
				divider		: true,
				name			: 'UI_ANOMALIE_INVENTARIO',
				icon			: 'fa-recycle',
				icon2			: 'fa-search',
				colorB		: '#0073b7',
				color			: 'bg-blue color-palette',
				label			: 'Gestione Anomalie Inventario',
				subLabel	:	'Visualizzazione e Risoluzione delle Eccezioni e delle Quarantene',
				url				:	'uiAnomalieInventario',
				visibled	:	true,
				disabled	:	false,
			},
			UI_NUOVO_INTERVENTO : {
				divider		: true,
				name			: 'UI_NUOVO_INTERVENTO',
				icon			: 'fa-edit',
				icon2			: '',
				colorB		: '#0073b7',
				color			: 'bg-blue color-palette',
				label			: 'Gui Nuovo Intervento',
				subLabel	:	'Collegamento alla Gui che permette di creare gli Interventi',
				url				:	'uiNuovoIntevento',
				visibled	:	false,
				disabled	:	false,
			}

	};


	$scope.showMenu = {
			UI_ANOMALIE_INVENTARIO		: $scope.keyIn === 'UI_ANOMALIE_INVENTARIO' ? true : false,
			UI_NUOVO_INTERVENTO		: $scope.keyIn === 'UI_NUOVO_INTERVENTO' ? true : false,
			MAIN									: $scope.keyIn === 'MAIN' ? true : false
	};
	
	
	
	
	
	/*gestione della visibilita */
	
	// se sei ROOT vedi e fai tutto 
	if ($scope.profilo.admin){
		
		$scope.isAbilitatoGui																		= true;
		$scope.showMenu.MAIN																		= true;
		
		$scope.showMenu.UI_ANOMALIE_INVENTARIO											= true;
		$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.visibled			= true;
		$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.disabled			= false;
		
		$scope.showMenu.UI_NUOVO_INTERVENTO											= false;
		$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= false;
		$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= true;
		
		/*$scope.showMenu.UI_NUOVO_INTERVENTO											= true;
		$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= true;
		$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= false;*/

	}
	else {

		/* controllo i gruppo della gui */
		
		$timeout(function() {
			
			$scope.getProfiloGUI();
			
			console.log('profilo');
			console.log($scope.profilo);
			
			/* tile Crea TRASFERIMENTO utilizzato dal MG e CM */
			if (($scope.profilo.MG) || ($scope.profilo.CM)) {
				$scope.isAbilitatoGui																		= true;
				$scope.showMenu.MAIN																		= true;
				
				$scope.showMenu.UI_ANOMALIE_INVENTARIO											= true;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.visibled			= true;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.disabled			= false;
				
				$scope.showMenu.UI_NUOVO_INTERVENTO											= false;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= false;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= true;
				
				/*$scope.showMenu.UI_NUOVO_INTERVENTO											= true;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= true;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= false;*/
				
				
			}
			/* tile Consegna Materiale utilizzato dal MAGAZZINIERE */
			/*else if ($scope.profilo.MG) { 
				$scope.isAbilitatoGui																		= true;
				$scope.showMenu.MAIN																		= true;

				$scope.showMenu.UI_ANOMALIE_INVENTARIO											= false;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.visibled			= false;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.disabled			= true;
				
				$scope.showMenu.UI_NUOVO_INTERVENTO											= true;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= true;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= false;
				
			}*/
			else{
				$scope.isAbilitatoGui																		= false;
				$scope.showMenu.MAIN																		= false;

				$scope.showMenu.UI_ANOMALIE_INVENTARIO											= false;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.visibled			= false;
				$scope.configMainMenu.UI_ANOMALIE_INVENTARIO.disabled			= true;

				$scope.showMenu.UI_NUOVO_INTERVENTO											= false;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.visibled			= false;
				$scope.configMainMenu.UI_NUOVO_INTERVENTO.disabled			= true;

				$state.go('notAuthorized');
			}

		},0);


	}
	
	
	
	
	
	
}

//controller utilizzato per il view activity con apertura modale dell'attività
function ActivityViewModalCtrl($scope, $state, $stateParams, $rootScope, $uibModalInstance, $sce, itemSelected,
		uiChiamante, uiConfig, tab, givenUrl) {

	$scope.stateParamsUI		= $stateParams;
	$scope.$ctrl.givenUrl = givenUrl;
	
	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	};
	
	$scope.$ctrl.item		= itemSelected;
	$scope.$ctrl.chiamante	= uiChiamante;
	$scope.$ctrl.config		= uiConfig;
	$scope.$ctrl.cssSmall	= 'small';
	$scope.$ctrl.tab		= tab;
	
	$scope.$ctrl.ok = function () {
		$uibModalInstance.close();
	};

	$scope.$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}


angular.module('guiPyrAnomalieInventario')
	.controller('ActivityViewModalCtrl', ActivityViewModalCtrl)
	.controller('guiPyrAnomalieInventarioCtrl', guiPyrAnomalieInventarioCtrl);